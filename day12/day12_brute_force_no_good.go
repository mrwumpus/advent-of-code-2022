package day12

import (
  "aoc/util/graph"
)

func compareShortest(paths... []*graph.Point) []*graph.Point {
  if len(paths) > 0 {
    first := paths[0]
    rest := paths[1:]

    shortest := compareShortest(rest...)
    if first == nil || (shortest != nil && len(shortest) < len(first)) {
      return shortest
    }

    return first
  } else {
    return nil
  }
}

func findShortestPath(start *graph.Point, end *graph.Point, heightMap graph.IntMatrix, curPath []*graph.Point) []*graph.Point {
  if start.InPath(curPath) {
    return nil
  }

  if start.Same(end) {
    return []*graph.Point{}
  }

  height, width := heightMap.Dimensions()

  up := start.Neighbor(graph.Up)
  right := start.Neighbor(graph.Right)
  down := start.Neighbor(graph.Down)
  left := start.Neighbor(graph.Left)

  var upPath, rightPath, downPath, leftPath []*graph.Point

  basePath := append(curPath, start)

  curHeight := heightMap.GetAtPoint(start)

  if up.WithinDimensions(height, width) && curHeight+1 >= heightMap.GetAtPoint(up) {
    upPath = findShortestPath(up, end, heightMap, basePath)
  }

  if right.WithinDimensions(height, width) && curHeight+1 >= heightMap.GetAtPoint(right) {
    rightPath = findShortestPath(right, end, heightMap, basePath)
  }

  if down.WithinDimensions(height, width) && curHeight+1 >= heightMap.GetAtPoint(down) {
    downPath = findShortestPath(down, end, heightMap, basePath)
  }

  if left.WithinDimensions(height, width) && curHeight+1 >= heightMap.GetAtPoint(left) {
    leftPath = findShortestPath(left, end, heightMap, basePath)
  }

  shortestPath := compareShortest(upPath, rightPath, downPath, leftPath)

  if shortestPath != nil {
    return append([]*graph.Point{start}, shortestPath...)
  }

  return nil
}

func ShortestPath(start *graph.Point, end *graph.Point, heightMap graph.IntMatrix) []*graph.Point {
  return findShortestPath(start, end, heightMap, []*graph.Point{})
}
