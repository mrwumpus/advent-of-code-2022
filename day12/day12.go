package day12

import (
  "aoc/util/graph"
)

type SearchNode struct {
  Point *graph.Point
  Parent *SearchNode
}

func pointAvailable(from *graph.Point, to *graph.Point, visited *map[string]bool, heightMap graph.IntMatrix, height, width int) bool {
  curHeight := heightMap.GetAtPoint(from)

  return ! (*visited)[to.Print()] && to.WithinDimensions(height, width) && curHeight+1 >= heightMap.GetAtPoint(to)
}

func addNode(p *graph.Point, parent *SearchNode, open []*SearchNode, visited *map[string]bool) []*SearchNode {
  child := SearchNode{p, parent}
  newOpen := append(open, &child)
  (*visited)[p.Print()] = true

  return newOpen
}

func search(open []*SearchNode, visited *map[string]bool, end *graph.Point, heightMap graph.IntMatrix, height, width int) *SearchNode {
  newOpen := []*SearchNode{}

  for _, node := range open {
    p := node.Point
    if p.Same(end) {
      return node
    }

    up := p.Neighbor(graph.Up)
    right := p.Neighbor(graph.Right)
    down := p.Neighbor(graph.Down)
    left := p.Neighbor(graph.Left)

    if pointAvailable(p, up, visited, heightMap, height, width) {
      newOpen = addNode(up, node, newOpen, visited)
    }
    if pointAvailable(p, right, visited, heightMap, height, width) {
      newOpen = addNode(right, node, newOpen, visited)
    }
    if pointAvailable(p, down, visited, heightMap, height, width) {
      newOpen = addNode(down, node, newOpen, visited)
    }
    if pointAvailable(p, left, visited, heightMap, height, width) {
      newOpen = addNode(left, node, newOpen, visited)
    }
  }

  if len(newOpen) > 0 {
    return search(newOpen, visited, end, heightMap, height, width)
  }

  return nil
}

func FindShortest(start *graph.Point, end *graph.Point, heightMap graph.IntMatrix) []*graph.Point {
  visited := map[string]bool{}
  visited[start.Print()] = true
  searchRoot := &SearchNode{start, nil}
  open := []*SearchNode{searchRoot}
  height, width := heightMap.Dimensions()

  endNode := search(open, &visited, end, heightMap, height, width)

  path := []*graph.Point{}

  for endNode != nil {
    path = append(path, endNode.Point)
    endNode = endNode.Parent
  }

  return path
}
