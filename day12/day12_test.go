package day12

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/graph"
  "aoc/util/test"
)

func convertAlphaHeight(str string) int {
  rs := []rune(str)
  r := rs[0]
  if r == 'S' {
    r = 'a'
  }

  if r == 'E' {
    r = 'z'
  }
  return int(r - 'a')
}

var testExample string = `Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi`

func findRune(strs []string, findRune rune) *graph.Point {
  for y, str := range strs {
    for x, r := range str {
      if r == findRune {
        return graph.NewPoint(x,y)
      }
    }
  }

  return nil
}

func findStarts(heightMap graph.IntMatrix) []*graph.Point {
  path := []*graph.Point{}

  height, width := heightMap.Dimensions()
  for j := 0; j < height; j++ {
    for i := 0; i < width; i++ {
      p := graph.NewPoint(i,j)
      if heightMap.GetAtPoint(p) == 0 {
        path = append(path, p)
      }
    }
  }

  return path
}

func TestExample(t *testing.T) {
  strs := strings.Split(testExample, "\n")
  heightMap := graph.NewMatrix(strs, convertAlphaHeight)

  start := findRune(strs, 'S')
  end := findRune(strs, 'E')

  shortest := FindShortest(start, end, heightMap)


  if len(shortest) - 1 != 31 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    strs := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        strs = append(strs, txt)
      }
    }

    heightMap := graph.NewMatrix(strs, convertAlphaHeight)

    start := findRune(strs, 'S')
    end := findRune(strs, 'E')

    /* Brute force wont work, do better
    shortest := ShortestPath(start, end, heightMap)
    graph.PrintPath(shortest)
    */

    shortest := FindShortest(start, end, heightMap)

    expectedLength := 391
    if len(shortest) - 1 != expectedLength {
      t.Errorf("Expected shortes path to be %d steps, but got %d", expectedLength, len(shortest) - 1)
    }
  })
}

func min(a int, b int) int {
  if a < b {
    return a
  }
  return b
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    strs := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        strs = append(strs, txt)
      }
    }

    heightMap := graph.NewMatrix(strs, convertAlphaHeight)

    end := findRune(strs, 'E')

    /* Brute force wont work, do better
    shortest := ShortestPath(start, end, heightMap)
    graph.PrintPath(shortest)
    */

    shortestPath := 0
    starts := findStarts(heightMap)

    //graph.PrintPath(starts)

    for _, start := range starts {
      shortest := FindShortest(start, end, heightMap)
      steps := len(shortest) - 1

      if steps > 0 {
        if shortestPath == 0 {
          shortestPath = steps
        } else {
          shortestPath = min(shortestPath, steps)
        }
      }

      /*
      if shortest != nil && len(shortest) > 0 {
        graph.PrintPath(shortest)
      } else {
        println("No path found...")
      }

      println()
      */
    }

    expectedLength := 386
    if shortestPath != expectedLength {
      t.Errorf("Expected shortes path to be %d steps, but got %d", expectedLength, shortestPath)
    }
  })
}
