package day4

import (
  "fmt"
)

type InclusiveRange struct {
  start, end int
}

func ReadInclusiveRange(r string) (rng *InclusiveRange, err error) {
  var start, end int
  _, scanErr := fmt.Sscanf(r, "%d-%d", &start, &end)
  if scanErr != nil {
    return nil, fmt.Errorf("Bad range: %v", scanErr)
  }
  return &InclusiveRange{start, end}, nil
}

func (incRange *InclusiveRange) Length() int {
  return incRange.end - incRange.start + 1
}

func (incRange *InclusiveRange) Superset(subRange *InclusiveRange) bool {
  return incRange.start <= subRange.start && incRange.end >= subRange.end
}

func CoversSameRange(incRangeOne *InclusiveRange, incRangeTwo *InclusiveRange) bool {
  if incRangeOne.Length() > incRangeTwo.Length() {
    return incRangeOne.Superset(incRangeTwo)
  } else {
    return incRangeTwo.Superset(incRangeOne)
  }
}

func min[T int | int64 | float64](a, b T) T {
  if a < b {
    return a
  }
  return b
}

func (incRange *InclusiveRange) Intersection(other *InclusiveRange) *InclusiveRange {
  if incRange.start >= other.start && incRange.start <= other.end {
    return &InclusiveRange{incRange.start, min(incRange.end, other.end)}
  }

  if other.start >= incRange.start && other.start <= incRange.end {
    return &InclusiveRange{other.start, min(other.end, incRange.end)}
  }

  return nil
}
