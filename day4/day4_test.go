package day4

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

func TestRangeLength(t *testing.T) {
  ranges := []struct {
    rangeStr string
    expectedLength int
  } {
    { "6-6", 1 },
    { "1065-1066", 2 },
    { "10-55", 46 },
  }

  for _, r := range ranges {
    inclusiveRange, err := ReadInclusiveRange(r.rangeStr)
    if err != nil {
      t.Errorf("%v", err)
    }

    length := inclusiveRange.Length()

    if length != r.expectedLength {
      t.Errorf("%s size should be %d, but got %d", r.rangeStr, r.expectedLength, length)
    }
  }
}

func parseAndTest[T any](rangesStr string, fn func(*InclusiveRange, *InclusiveRange) T) T {
  strRanges := strings.Split(rangesStr, ",")
  first, err := ReadInclusiveRange(strRanges[0])
  if err != nil {
    panic(err)
  }
  second, err := ReadInclusiveRange(strRanges[1])
  if err != nil {
    panic(err)
  }

  return fn(first, second)
}

func parseAndTestIsContained(rangesStr string) bool {
  return parseAndTest(rangesStr, CoversSameRange)
}

func TestExampleRanges(t *testing.T) {
  ranges := []struct {
    ranges string
    isSubset bool
  } {
    {"2-4,6-8", false },
    {"2-3,4-5", false },
    {"5-7,7-9", false },
    {"2-8,3-7", true },
    {"6-6,4-6", true },
    {"2-6,4-8", false },
  }

  for _,r := range ranges {
    isContained := parseAndTestIsContained(r.ranges)
    if isContained != r.isSubset {
      t.Errorf("%s isSubset is %t, but got %t", r.ranges, r.isSubset, isContained)
    }
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    var totalCount int

    for scanner.Scan() {
      txt := scanner.Text()

      if parseAndTestIsContained(txt) {
        totalCount++
      }
    }

    expectedCount := 560
    if totalCount != expectedCount {
      t.Errorf("Was expecing %d, but found %d", expectedCount, totalCount)
    }
  })
}

func parseAndTestHasIntersection(rangesStr string) bool {
  return parseAndTest(rangesStr, func(first *InclusiveRange, other *InclusiveRange) bool {
    return first.Intersection(other) != nil
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    var totalCount int

    for scanner.Scan() {
      txt := scanner.Text()
      if parseAndTestHasIntersection(txt) {
        totalCount++
      }
    }

    expectedCount := 839
    if totalCount != expectedCount {
      t.Errorf("Was expecing %d, but found %d", expectedCount, totalCount)
    }
  })
}
