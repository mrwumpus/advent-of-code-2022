package day8

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

var testMatrix = []string {
  "30373",
  "25512",
  "65332",
  "33549",
  "35390",
}

func TestExample(t *testing.T) {
  matrix := NewMatrix(testMatrix)
  height, width := matrix.Dimensions()

  if height != 5 || width != 5 {
    t.Fail()
  }

  count := matrix.CountVisibleTrees()

  if count != 21 {
    t.Errorf("Expected to count 21 trees, but counted %d", count)
  }

  scenicScore := matrix.CalcScenicScore(1, 2)

  if scenicScore != 4 {
    t.Fail()
  }

  scenicScore = matrix.CalcScenicScore(3, 2)

  if scenicScore != 8 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    topology := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        topology = append(topology, txt)
      }
    }

    matrix := NewMatrix(topology)

    count := matrix.CountVisibleTrees()

    expectedCount := 1708
    if count != expectedCount {
      t.Errorf("Expected to count %d trees, but counted %d", expectedCount, count)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    topology := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        topology = append(topology, txt)
      }
    }

    matrix := NewMatrix(topology)

    maxScore := matrix.MaxScenicScore()

    expectedScore := 504000
    if maxScore != expectedScore {
      t.Errorf("Expected to score %d, but scored %d", expectedScore, maxScore)
    }
  })
}
