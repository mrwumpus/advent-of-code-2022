package day8

import (
  "strings"
  "fmt"
)

type IntMatrix [][]int

func min(vals ...int) int {
  a := vals[0]
  if len(vals) == 1 {
    return a
  }

  b := min(vals[1:]...)
  if a > b {
    return b
  }

  return a
}

func max(vals ...int) int {
  a := vals[0]
  if len(vals) == 1 {
    return a
  }

  b := max(vals[1:]...)
  if a > b {
    return a
  }

  return b
}

func ExplodeIntString(str string) []int {
  ints := []int{}

  var val int
  for _, char := range strings.Split(str, "") {
    fmt.Sscanf(char, "%d", &val)
    ints = append(ints, val)
  }

  return ints
}

func NewMatrix(vals []string) *IntMatrix {
  newMatrix := make(IntMatrix, len(vals))
  for i, rowStr := range vals {
    newMatrix[i] = ExplodeIntString(rowStr)
  }

  return &newMatrix
}

func (matrix *IntMatrix) Dimensions() (height int, width int) {
  height = len(*matrix)

  for _, row := range *matrix {
    width = max(width, len(row))
  }

  return
}

func (matrix *IntMatrix) findLocation(rowIdx int, colIdx int) *int {
  height := len(*matrix)
  if rowIdx >= height || rowIdx < 0 {
    return nil
  }

  row := (*matrix)[rowIdx]

  width := len(row)

  if colIdx >= width || colIdx < 0 {
    return nil
  }

  return &row[colIdx]
}

func (matrix *IntMatrix) Get(rowIdx int, colIdx int) int {
  valLoc := matrix.findLocation(rowIdx, colIdx)
  if valLoc != nil {
    return *valLoc
  } else {
    return -1
  }
}

func (matrix *IntMatrix) Put(rowIdx int, colIdx int, val int) {
  valLoc := matrix.findLocation(rowIdx, colIdx)
  if valLoc != nil {
    *valLoc = val
  } else {
    panic(fmt.Sprintf("Unable to set value at %d,%d", rowIdx, colIdx))
  }
}

func (matrix *IntMatrix) initNew() *IntMatrix {
  height, width := matrix.Dimensions()

  newMatrix := make(IntMatrix, height)

  for i := 0; i < height; i++ {
    newMatrix[i] = make([]int, width)
  }

  return &newMatrix
}

func (matrix *IntMatrix) MaxFromNorth() *IntMatrix {
  height, width := matrix.Dimensions()
  northMatrix := matrix.initNew()

  var currentMax int
  for colIdx := 0; colIdx < width; colIdx++ {
    currentMax = -1
    for rowIdx := 0; rowIdx < height; rowIdx++ {
      northMatrix.Put(rowIdx, colIdx, currentMax)

      currentHeight := matrix.Get(rowIdx, colIdx)
      currentMax = max(currentMax, currentHeight)
    }
  }

  return northMatrix
}

func (matrix *IntMatrix) MaxFromWest() *IntMatrix {
  height, width := matrix.Dimensions()
  westMatrix := matrix.initNew()

  var currentMax int
  for rowIdx := 0; rowIdx < height; rowIdx++ {
    currentMax = -1
    for colIdx := 0; colIdx < width; colIdx++ {
      westMatrix.Put(rowIdx, colIdx, currentMax)

      currentHeight := matrix.Get(rowIdx, colIdx)
      currentMax = max(currentMax, currentHeight)
    }
  }

  return westMatrix
}

func (matrix *IntMatrix) MaxFromSouth() *IntMatrix {
  height, width := matrix.Dimensions()
  southMatrix := matrix.initNew()

  var currentMax int
  for colIdx := 0; colIdx < width; colIdx++ {
    currentMax = -1
    for rowIdx := height - 1; rowIdx >= 0; rowIdx-- {
      southMatrix.Put(rowIdx, colIdx, currentMax)

      currentHeight := matrix.Get(rowIdx, colIdx)
      currentMax = max(currentMax, currentHeight)
    }
  }

  return southMatrix
}

func (matrix *IntMatrix) MaxFromEast() *IntMatrix {
  height, width := matrix.Dimensions()
  eastMatrix := matrix.initNew()

  var currentMax int
  for rowIdx := 0; rowIdx < height; rowIdx++ {
    currentMax = -1
    for colIdx := width - 1; colIdx >= 0; colIdx-- {
      eastMatrix.Put(rowIdx, colIdx, currentMax)

      currentHeight := matrix.Get(rowIdx, colIdx)
      currentMax = max(currentMax, currentHeight)
    }
  }

  return eastMatrix
}

func (matrix *IntMatrix) CountVisibleTrees () (treeCount int) {
  northMap := matrix.MaxFromNorth()
  southMap := matrix.MaxFromSouth()
  eastMap := matrix.MaxFromEast()
  westMap := matrix.MaxFromWest()

  height, width := matrix.Dimensions()

  for rowIdx := 0; rowIdx < height; rowIdx++ {
    for colIdx := 0; colIdx < width; colIdx++ {
      currentHeight := matrix.Get(rowIdx, colIdx)

      minViewHeight := min(northMap.Get(rowIdx, colIdx),
                           southMap.Get(rowIdx, colIdx),
                           eastMap.Get(rowIdx, colIdx),
                           westMap.Get(rowIdx, colIdx))

      //fmt.Printf("Min view at %d,%d is %d. Comparing to %d\n", rowIdx, colIdx, minViewHeight, currentHeight )
      if minViewHeight < currentHeight {
        treeCount++
      }
    }
  }

  return
}

func (matrix *IntMatrix) northScore (rowIdx int, colIdx int) (score int) {
  currentVal := matrix.Get(rowIdx, colIdx)

  sightVal := 0
  for i := 1; sightVal >= 0; i++ {
    sightVal := matrix.Get(rowIdx - i, colIdx)
    if sightVal < 0 {
      break
    }

    score++

    if sightVal >= currentVal {
      break
    }
  }

  return
}

func (matrix *IntMatrix) southScore (rowIdx int, colIdx int) (score int) {
  currentVal := matrix.Get(rowIdx, colIdx)

  sightVal := 0
  for i := 1; sightVal >= 0; i++ {
    sightVal := matrix.Get(rowIdx + i, colIdx)
    if sightVal < 0 {
      break
    }

    score++

    if sightVal >= currentVal {
      break
    }
  }

  return
}

func (matrix *IntMatrix) eastScore (rowIdx int, colIdx int) (score int) {
  currentVal := matrix.Get(rowIdx, colIdx)

  sightVal := 0
  for i := 1; sightVal >= 0; i++ {
    sightVal := matrix.Get(rowIdx, colIdx + i)
    if sightVal < 0 {
      break
    }

    score++

    if sightVal >= currentVal {
      break
    }
  }

  return
}

func (matrix *IntMatrix) westScore (rowIdx int, colIdx int) (score int) {
  currentVal := matrix.Get(rowIdx, colIdx)

  sightVal := 0
  for i := 1; sightVal >= 0; i++ {
    sightVal := matrix.Get(rowIdx, colIdx - i)
    if sightVal < 0 {
      break
    }

    score++

    if sightVal >= currentVal {
      break
    }
  }

  return
}

func (matrix *IntMatrix) CalcScenicScore (rowIdx int, colIdx int) int {
  return matrix.northScore(rowIdx, colIdx) *
         matrix.eastScore(rowIdx, colIdx) *
         matrix.southScore(rowIdx, colIdx) *
         matrix.westScore(rowIdx, colIdx)
}

func (matrix *IntMatrix) MaxScenicScore() (maxScore int) {
  height, width := matrix.Dimensions()

  for rowIdx := 0; rowIdx < height; rowIdx++ {
    for colIdx := 0; colIdx < width; colIdx++ {
      maxScore = max(maxScore, matrix.CalcScenicScore(rowIdx, colIdx))
    }
  }
  return
}
