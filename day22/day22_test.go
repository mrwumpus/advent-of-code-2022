package day22

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

const exampleInput = `        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5`

func TestExample(t *testing.T) {
  worldLines := []string{}
  var commands string

  parseCommand := false
  for _, line := range strings.Split(exampleInput, "\n") {
    if line == "" {
      parseCommand = true
    } else {
      if parseCommand {
        commands = line
      } else {
        worldLines = append(worldLines, line)
      }
    }
  }

  world := ParseWorld(worldLines)
  world.Execute(commands)
  score := world.traveler.Score()

  if score != 6032 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    worldLines := []string{}
    var commands string

    parseCommand := false
    for scanner.Scan() {
      line := scanner.Text()
      if line == "" {
        parseCommand = true
      } else {
        if parseCommand {
          commands = line
        } else {
          worldLines = append(worldLines, line)
        }
      }
    }

    world := ParseWorld(worldLines)
    //world.Print()
    world.Execute(commands)
    //println(commands)
    score := world.traveler.Score()

    expected := 87231
    if score != expected {
      t.Errorf("Expected traveler score of %d, but got %d", expected, score)
    }
  })
}

