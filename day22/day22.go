package day22

import (
  "fmt"
  "aoc/util"
  "aoc/util/graph"
)

type Region struct {
  minp, maxp *graph.Point
}

type Traveler struct {
  direction graph.Direction
  position *graph.Point
}

func scoreDirection(dir graph.Direction) int {
  switch dir {
  case graph.Right: return 0
  case graph.Down: return 1
  case graph.Left: return 2
  default: return 3
  }
}

func (t *Traveler)Score() int {
  return 1000 * (t.position.Y() + 1) + 4 * (t.position.X() + 1) + scoreDirection(t.direction)
}

type World struct {
  bounds *Region
  regions []*Region
  walls []*graph.Point
  traveler *Traveler
}

func (w *World)AddWalls(walls []*graph.Point) {
  w.walls = append(w.walls, walls...)
}

func (w *World)AddRegion(minX, minY, maxX, maxY int) {
  maxp := graph.NewPoint(maxX, maxY)
  region := Region{graph.NewPoint(minX, minY), maxp}
  w.regions = append(w.regions, &region)
  if w.bounds.maxp.X() < maxX || w.bounds.maxp.Y() < maxY {
    w.bounds.maxp = graph.NewPoint(util.Max(w.bounds.maxp.X(), maxX), util.Max(w.bounds.maxp.Y(), maxY))
  }
}

func (w *World)InBounds(p *graph.Point) bool {
  for _, region := range w.regions {
    if p.InBounds(region.minp, region.maxp) {
      return true
    }
  }
  return false
}

func (w *World)topMost(x int) *graph.Point {
  cur := graph.NewPoint(x, 0)
  for !w.InBounds(cur) {
    cur = cur.Neighbor(graph.Down)
  }
  return cur
}

func (w *World)bottomMost(x int) *graph.Point {
  cur := graph.NewPoint(x, w.bounds.maxp.Y())
  for !w.InBounds(cur) {
    cur = cur.Neighbor(graph.Up)
  }
  return cur
}

func (w *World)leftMost(y int) *graph.Point {
  cur := graph.NewPoint(0, y)
  for !w.InBounds(cur) {
    cur = cur.Neighbor(graph.Right)
  }
  return cur
}

func (w *World)rightMost(y int) *graph.Point {
  cur := graph.NewPoint(w.bounds.maxp.X(), y)
  for !w.InBounds(cur) {
    cur = cur.Neighbor(graph.Left)
  }
  return cur
}

func (w *World)Neighbor(p *graph.Point, dir graph.Direction) *graph.Point {
  n := p.Neighbor(dir)
  if w.InBounds(n) {
    return n
  }

  switch dir {
  case graph.Right:
    return w.leftMost(p.Y())
  case graph.Down:
    return w.topMost(p.X())
  case graph.Left:
    return w.rightMost(p.Y())
  default:
    return w.bottomMost(p.X())
  }
}

func (w *World)Print() {
  for y := 0; y <= w.bounds.maxp.Y(); y++ {
    for x := 0; x <= w.bounds.maxp.X(); x++ {
      cur := graph.NewPoint(x,y)
      if cur.Same(w.traveler.position) {
        switch w.traveler.direction {
        case graph.Up:
          print("^")
        case graph.Down:
          print("v")
        case graph.Right:
          print(">")
        default:
          print("<")
        }
      } else if cur.InPath(w.walls) {
        print("#")
      } else if w.InBounds(cur) {
        print(".")
      } else {
        print(" ")
      }
    }
    println()
  }
}

func ParseWorldLine(line string, y int) (minX, maxX int, walls []*graph.Point) {
  maxX = len(line) - 1

  walls = []*graph.Point{}

  minX = -1
  for x := 0; x < len(line); x++ {
    ch := line[x]
    if ch != ' ' && minX < 0 {
      minX = x
    }

    if ch == '#' {
      walls = append(walls, graph.NewPoint(x, y))
    }
  }

  return
}

func ParseWorld(lines []string) *World {
  traveler := Traveler{direction: graph.Right}
  worldRegion := Region{graph.NewPoint(0,0), graph.NewPoint(0,0)}
  world := World{&worldRegion, []*Region{}, []*graph.Point{}, &traveler}

  var curMinX, curMaxX int
  var curMinY, curMaxY int
  for y, line := range lines {
    minX, maxX, walls := ParseWorldLine(line, y)
    world.AddWalls(walls)
    if traveler.position == nil {
      traveler.position = graph.NewPoint(minX, y)
      curMinX = minX
      curMaxX = maxX
    } else {
      if minX != curMinX || maxX != curMaxX {
        world.AddRegion(curMinX, curMinY, curMaxX, curMaxY)
        curMinY = y
        curMinX = minX
        curMaxX = maxX
      }
      curMaxY = y
    }
  }
  world.AddRegion(curMinX, curMinY, curMaxX, curMaxY)

  return &world
}

func (w *World)Travel(distance int) {
  t := w.traveler
  for i := 0; i < distance; i++ {
    next := w.Neighbor(t.position, t.direction)
    if next.InPath(w.walls) {
      //println("OOF!")
      return
    }

    t.position = next
  }
}

func (w *World)Turn(rotation rune) {
  next := w.traveler.direction

  if rotation == 'R' {
    switch next {
    case graph.Up:
      next = graph.Right
    case graph.Down:
      next = graph.Left
    case graph.Right:
      next = graph.Down
    default:
      next = graph.Up
    }
  } else {
    switch next {
    case graph.Up:
      next = graph.Left
    case graph.Down:
      next = graph.Right
    case graph.Right:
      next = graph.Up
    default:
      next = graph.Down
    }
  }

  w.traveler.direction = next
}

func (w *World)Execute(commands string) {
  var prevIdx, distance int
  for i, ch := range commands {
    if ch == 'L' || ch == 'R' {
      fmt.Sscanf(commands[prevIdx:i], "%d", &distance)
      //fmt.Printf("Hey, walk %d steps, then turn %c\n", distance, ch)
      w.Travel(distance)
      w.Turn(ch)
      prevIdx = i+1
    }
  }
  fmt.Sscanf(commands[prevIdx:], "%d", &distance)
  w.Travel(distance)
  //fmt.Printf("Now just walk %d more steps\n", distance)
}
