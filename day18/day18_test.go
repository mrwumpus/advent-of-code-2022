package day18

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/g3d"
  "aoc/util/test"
)

const (
  exampleInput = `2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5`
)

func TestExample(t *testing.T) {
  g := g3d.NewGrid()

  for _, line := range strings.Split(exampleInput, "\n") {
    p := g3d.ParsePoint(line)
    g.AddPoint(p)
  }

  count := CountFaces(g)

  expected := 64
  if count != expected {
    t.Errorf("Expected %d; got %d", expected, count)
  }

  count = g.ShellCount()

  expected = 58
  if count != expected {
    t.Errorf("Expected %d; got %d", expected, count)
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    g := g3d.NewGrid()
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        p := g3d.ParsePoint(txt)
        g.AddPoint(p)
      }
    }

    count := CountFaces(g)
    expected := 4636
    if count != expected {
      t.Errorf("Expected %d; got %d", expected, count)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    g := g3d.NewGrid()
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        p := g3d.ParsePoint(txt)
        g.AddPoint(p)
      }
    }

    count := g.ShellCount()
    expected := 2572
    if count != expected {
      t.Errorf("Expected %d; got %d", expected, count)
    }
  })
}
