package day18

import (
  "aoc/util/g3d"
)

func CountFaces(g *g3d.Grid) int {
  var prev *g3d.Point

  direction := g3d.In

  var count int

  g.WalkFaces(func(p *g3d.Point) {
    if p == nil {
      if prev != nil {
        count++
      }

      prev = nil
      if direction == g3d.In {
        direction = g3d.Left
      } else {
        direction = g3d.Down
      }
      return
    }

    if prev == nil {
      count++
    } else if ! p.Neighbor(direction).Same(prev) {
      count+=2
    }
    prev = p
  })

  return count
}

