package day20

import (
  "math"
)

type NumTrain struct {
  engine *NumTrainCar
  length int
}

func MakeTrain() *NumTrain {
  return &NumTrain{}
}

type NumTrainCar struct {
  num int
  prev, next *NumTrainCar
  train *NumTrain
}

func (t *NumTrain)Add(num int) {
  car := NumTrainCar{num: num, train: t}
  if t.engine == nil {
    car.next = &car
    car.prev = &car
    t.engine = &car
  } else {
    end := t.engine.prev

    t.engine.prev = &car
    end.next = &car
    car.prev = end
    car.next = t.engine
  }
  t.length++
}


func (c *NumTrainCar)Move(length int) {
  if length != 0 && c.train.engine == c {
    c.train.engine = c.next
  }

  if length < 0 {
    // go back
    for i := length; i < 0; i++ {
      newPrev := c.prev.prev
      c.prev.prev.next = c
      c.prev.prev = c
      c.prev.next = c.next
      c.next.prev = c.prev
      c.next = c.prev
      c.prev = newPrev
    }
  } else {
    // go forward
    for i := 0; i < length; i++ {
      newNext := c.next.next
      c.next.next.prev = c
      c.next.next = c
      c.next.prev = c.prev
      c.prev.next = c.next
      c.prev = c.next
      c.next = newNext
    }
  }
}

func (t *NumTrain)Move(car *NumTrainCar, length int) {
  mod := math.Mod(float64(length), float64(t.length-1))
  car.Move(int(mod))
}

func (t *NumTrain)Find(val int) (car *NumTrainCar) {
  car = t.engine
  for {
    if car.num == val {
      return
    }
    car = car.next

    if car == t.engine {
      return nil
    }
  }
}

func (t *NumTrain)Seek(car *NumTrainCar, length int) (res *NumTrainCar) {
  res = car
  mod := math.Mod(float64(length), float64(t.length))
  for i := 0; i < int(mod); i++ {
    res = res.next
  }

  return
}

func (t *NumTrain)Print() {
  car := t.engine
  for {
    print(car.num, ",")
    car = car.next

    if car == t.engine {
      println()
      break
    }
  }
}

func (t *NumTrain)Slice() (slice []*NumTrainCar) {
  slice = make([]*NumTrainCar, t.length)
  car := t.engine
  for i := 0; i < t.length; i++ {
    slice[i] = car
    car = car.next
  }
  return
}

func (t *NumTrain)FindCoords() (x, y, z int) {
  car := t.Find(0)
  car = t.Seek(car, 1000)
  x = car.num
  car = t.Seek(car, 1000)
  y = car.num
  car = t.Seek(car, 1000)
  z = car.num
  return
}

func (t *NumTrain)SumCoords() (sum int) {
  x, y, z := t.FindCoords()
  return x + y + z
}

func (t *NumTrain)Mix(times int) {
  slice := t.Slice()
  for i := 0; i < times; i++ {
    for _, car := range slice {
      //println("moving", car.num)
      t.Move(car, car.num)
      //train.Print()
    }
  }
}
