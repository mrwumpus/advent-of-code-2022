package day20

import (
  "bufio"
  "fmt"
  "strings"
  "testing"
  "aoc/util/test"
)

const exampleFile = `1
2
-3
3
-2
0
4`

const key = 811589153

func TestExample(t *testing.T) {
  train := MakeTrain()
  for _, str := range strings.Split(exampleFile, "\n") {
    var num int
    fmt.Sscanf(str, "%d", &num)
    train.Add(num)
  }

  train.Mix(1)

  sum := train.SumCoords()
  if sum != 3 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    train := MakeTrain()
    var num int
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        fmt.Sscanf(txt, "%d", &num)
        train.Add(num)
      }
    }

    if train.length != 5000 {
      t.Errorf("Train langth should be 5000, but it's %d", train.length)
    }

    // Mix
    train.Mix(1)
    sum := train.SumCoords()
    expectedSum := 8028
    if sum >= 8494 {
      t.Errorf("Sum >= 8494 is too high: %d", sum)
    } else {
      if sum != expectedSum {
        t.Errorf("Expected coordinate sum to be %d, but got %d", expectedSum, sum)
      }
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    train := MakeTrain()
    var num int
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        fmt.Sscanf(txt, "%d", &num)
        train.Add(num * key)
      }
    }

    if train.length != 5000 {
      t.Errorf("Train length should be 5000, but it's %d", train.length)
    }

    // Mix
    train.Mix(10)
    sum := train.SumCoords()
    expectedSum := 8798438007673
    if sum <= 5730631009333 {
      t.Errorf("Sum is too low: %d", sum)
    } else {
      if sum != expectedSum {
        t.Errorf("Expected coordinate sum to be %d, but got %d", expectedSum, sum)
      }
    }
  })
}
