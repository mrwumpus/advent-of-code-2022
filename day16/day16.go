package day16

import (
  "fmt"
  "regexp"
  "strings"
)

type Cavern struct {
  name string
  pressure int
  children []*Cavern
}

func ParseCavern (input string) *Cavern {
  var name string
  var valveRate int

  fmt.Sscanf(input, "Valve %s has flow rate=%d", &name, &valveRate)

  return &Cavern{name, valveRate, []*Cavern{}}
}

func ParseCavernChildren(input string, caverns map[string]*Cavern) {
  var name string

  fmt.Sscanf(input, "Valve %s", &name)

  curCavern := caverns[name]

  re := regexp.MustCompile(`[A-Z]{2}`)
  childNames := re.FindAll([]byte(strings.Split(input, ";")[1]), -1)

  for _ , name := range childNames {
    curCavern.children = append(curCavern.children, caverns[string(name)])
  }
}

type SearchNode struct {
  name string
  parent *SearchNode
  length int
}

func findShortest(length int, open []*SearchNode, to string, caverns map[string]*Cavern, visited map[string]bool) *SearchNode {
  nextOpen := []*SearchNode{}

  for _, sn := range open {
    if sn.name == to {
      return sn
    }

    from := caverns[sn.name]

    for _, child := range from.children {
      if !visited[child.name] {
        visited[child.name] = true
        node := SearchNode{child.name, sn, length + 1}
        nextOpen = append(nextOpen, &node)
      }
    }
  }

  if len(nextOpen) > 0 {
    return findShortest(length + 1, nextOpen, to, caverns, visited)
  }

  return nil
}

func ShortestPath(from string, to string, caverns map[string]*Cavern) []string {
  visited := map[string]bool{}
  visited[from] = true

  open := []*SearchNode{&SearchNode{from, nil, 1}}

  shortest := findShortest(1, open, to, caverns, visited)

  path := make([]string, shortest.length)
  current := shortest
  idx := shortest.length - 1
  for current != nil {
    path[idx] = current.name
    idx--
    current = current.parent
  }

  return path
}

func CavernsWithPressure(caverns map[string]*Cavern) []string {
  pcs := []string{}
  for name, c := range caverns {
    if c.pressure > 0 {
      pcs = append(pcs, name)
    }
  }

  return pcs
}

type Edge struct {
  from, to string
  cost, benefit int
}

func createEdge(from string, to string, caverns map[string]*Cavern) *Edge {
  shortest := ShortestPath(from, to, caverns)
  benefit := caverns[to].pressure
  return &Edge{from, to, len(shortest), benefit}
}

func GatherEdges(caverns map[string]*Cavern) []*Edge {
  nodes := CavernsWithPressure(caverns)

  edges := []*Edge{}
  for _, n := range nodes {
    for _, n2 := range nodes {
      if n != n2 {
        edges = append(edges, createEdge(n, n2, caverns))
      }
    }
  }

  for _, c := range nodes {
    edges = append(edges, createEdge("AA", c, caverns))
  }

  return edges
}

func getAvailableEdges(from string, edges []*Edge) []*Edge {
  next := []*Edge{}
  for _, e := range edges {
    if e.from == from {
         next = append(next, e)
     }
  }

  return next
}

func removeEdgesForName(name string, edges []*Edge) []*Edge {
  next := []*Edge{}
  for _, e := range edges {
    if e.from != name && e.to != name {
      next = append(next, e)
    }
  }

  return next
}

func ScoreEdgePath(path []*Edge, limit int, verbose bool) int {
  var edgeIdx, totalBenefit, curBenefit, curEdgeCount int
  var curEdge *Edge
  for i := 1; i <= limit; i++ {
    if curEdge == nil && edgeIdx < len(path) {
      curEdge = path[edgeIdx]
      curEdgeCount = 1
      edgeIdx++
    }

    totalBenefit += curBenefit

    if verbose {
      println(i, "Current benefit", curBenefit, "added to total:", totalBenefit)
    }

    if curEdge != nil {
      if curEdgeCount == curEdge.cost {
        if verbose {
          println(i, "Releasing valve worth", curEdge.benefit)
        }
        curBenefit += curEdge.benefit
        curEdge = nil
      } else {
        if verbose {
          println(i, "Moving between", curEdge.from, "and", curEdge.to)
        }
        curEdgeCount++
      }
    }
  }

  return totalBenefit
}

func PrintEdgePath(path []*Edge) {
  for i, edge := range path {
    print(edge.from, "->")
    if i == len(path) - 1 {
      println(edge.to)
    }
  }
}

func PrintEdges(edges []*Edge) {
  for _, edge := range edges {
    fmt.Printf("From %s to %s will cost %d but release %d\n", edge.from, edge.to, edge.cost, edge.benefit)
  }
  println()
}

func scoreEdgesRecur(path []*Edge, currentName string, currentEdge *Edge, edges []*Edge, limit int, scoreLimit int, currentBest *int, bestPath *[]*Edge) {
  next := getAvailableEdges(currentName, edges)
  remaining := removeEdgesForName(currentName, edges)

  var name string
  var childCount int
  for _, n := range next {
    if limit >= n.cost {
      childCount++
      name = n.to

      newPath := append(path, n)
      scoreEdgesRecur(newPath, name, n, remaining, limit - n.cost, scoreLimit, currentBest, bestPath)
    }
  }

  if childCount == 0 {
    newBest := ScoreEdgePath(path, scoreLimit, false)
    if newBest > *currentBest {
      *currentBest = newBest
      *bestPath = path
    }
  }
}

func ScoreEdges(edges []*Edge, limit int) int {
  start := "AA"

  next := getAvailableEdges(start, edges)
  remaining := removeEdgesForName(start, edges)
  var currentBest int
  var bestPath []*Edge
  for _, n := range next {
    if limit >= n.cost {
      scoreEdgesRecur([]*Edge{n}, n.to, n, remaining, limit - n.cost, limit, &currentBest, &bestPath)
    }
  }

  //PrintEdges(bestPath)
  //ScoreEdgePath(bestPath, 30, true)
  //println(currentBest)
  return currentBest
}

func DualWalk(edges []*Edge, limit int) (score int) {
  start := "AA"

  next := getAvailableEdges(start, edges)
  remaining := removeEdgesForName(start, edges)

  for i := 0; i < len(next); i++ {
    for j := i + 1; j < len(next); j++ {
      visited := map[string]bool{}

      e1 := next[i]
      p1 := []*Edge{e1}
      visited[e1.to] = true
      time1 := e1.cost

      e2 := next[j]
      p2 := []*Edge{e2}
      visited[e2.to] = true
      time2 := e2.cost

      elephantWalk(p1, e1, time1, p2, e2, time2, &visited, remaining, limit, &score)
    }
  }

  return
}

func elephantWalk(
  p1 []*Edge, e1 *Edge, time1 int,
  p2 []*Edge, e2 *Edge, time2 int,
  visited *map[string]bool, remaining []*Edge, limit int, maxScore *int) {

    walkedChild := false

    nexts := getAvailableEdges(e1.to, remaining)
    remains := removeEdgesForName(e1.to, remaining)

    for _, n := range nexts {
      if n.to != e2.to && time1 + n.cost <= limit {
        newPath := append(p1, n)
        (*visited)[n.to] = true
        elephantWalk(newPath, n, time1 + n.cost, p2, e2, time2, visited, remains, limit, maxScore)
        walkedChild = true
      }
    }

    nexts2 := getAvailableEdges(e2.to, remains)
    remains = removeEdgesForName(e2.to, remains)
    for _, n2 := range nexts2 {
      if time2 + n2.cost <= limit {
        newPath2 := append(p2, n2)
        (*visited)[n2.to] = true
        elephantWalk(p1, e1, time1, newPath2, n2, time2 + n2.cost, visited, remains, limit, maxScore)
        walkedChild = true
      }
    }

    if len(remaining) == 0 || !walkedChild {
      /*
      println("Got Paths:")
      PrintEdgePath(p1)
      PrintEdgePath(p2)
      */
      val1 := ScoreEdgePath(p1, limit, false)
      val2 := ScoreEdgePath(p2, limit, false)
      score := val1 + val2
      //println("Total:", val1+val2)

      if score > *maxScore {
        *maxScore = score
        // Well, this cheat managed to work... lol
        println("Current MAX:", score)
      }

    }
}
