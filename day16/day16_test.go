package day16

import (
  "bufio"
  "fmt"
  "strings"
  "testing"
  "aoc/util/test"
)

var testInput = `Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II`

func printCaverns(cs map[string]*Cavern) {
  for _, c := range cs {
    fmt.Printf("%s with flow rate %d\n", c.name, c.pressure)
    if len(c.children) == 0 {
      println("  Leads nowhere")
    } else {
      print("   Leads to ")
      for i, d := range c.children {
        if i > 0 {
          print(", ")
        }
        print(d.name)
      }
      println()
    }
  }
}

func TestExample(t *testing.T) {
  caverns := map[string]*Cavern{}

  for _, input := range strings.Split(testInput, "\n") {
    cavern := ParseCavern(input)
    caverns[cavern.name] = cavern
  }

  for _, input := range strings.Split(testInput, "\n") {
    ParseCavernChildren(input, caverns)
  }

  //printCaverns(caverns)

  edges := GatherEdges(caverns)
  //PrintEdges(edges)
  val := ScoreEdges(edges, 30)
  if val != 1651 {
    t.Fail()
  }

  score := DualWalk(edges, 26)
  if score != 1707 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    input := []string{}

    caverns := map[string]*Cavern{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        input = append(input, txt)
        cavern := ParseCavern(txt)
        caverns[cavern.name] = cavern
      }
    }

    for _, line := range input {
      ParseCavernChildren(line, caverns)
    }
    //printCaverns(caverns)

    edges := GatherEdges(caverns)
    //PrintEdges(edges)

    val := ScoreEdges(edges, 30)

    expectedRelease := 1947
    if val != expectedRelease {
      t.Errorf("Expected total relief of %d but got %d", expectedRelease, val)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    input := []string{}

    caverns := map[string]*Cavern{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        input = append(input, txt)
        cavern := ParseCavern(txt)
        caverns[cavern.name] = cavern
      }
    }

    for _, line := range input {
      ParseCavernChildren(line, caverns)
    }

    edges := GatherEdges(caverns)

    //PrintEdges(edges)

    val := DualWalk(edges, 26)

    expectedRelease := 1947
    if val != expectedRelease {
      t.Errorf("Expected total relief with elephant of %d but got %d", expectedRelease, val)
    }
  })
}

