package day13

import (
  "bufio"
  "fmt"
  "strings"
)

func IsList(packet string) bool {
  return strings.HasPrefix(packet, "[")
}

func min(a, b int) int {
  if a < b {
    return a
  }
  return b
}

func listify(packet string) []string {
  if packet == "[]" {
    return []string{}
  }

  trimmed := packet[1:len(packet)-1]

  openCount := 0
  splits := []string{}
  start := 0
  for i, ch := range trimmed {
    if ch == '[' {
      openCount++
    } else if ch == ']' {
      openCount--
    } else if ch == ',' && openCount == 0 {
      splits = append(splits, trimmed[start:i])
      start = i+1
    }
  }

  splits = append(splits, trimmed[start:])

  //fmt.Printf("split %s to %v\n\n", trimmed, splits)

  return splits
}

func ParsePairs(scanner *bufio.Scanner) []*[]string {
  pairs := []*[]string{}
  var firstLine string
  for scanner.Scan() {
    txt := scanner.Text()
    if txt != "" {
      if firstLine != "" {
        pairs = append(pairs, &[]string{ firstLine, txt })
        firstLine = ""
      } else {
        firstLine = txt
      }
    }
  }

  return pairs
}

func ComparePackets(left, right string) int {
  if IsList(left) && IsList(right) {
    leftList := listify(left)
    rightList := listify(right)

    var comparison int
    for i := 0; i < min(len(leftList), len(rightList)); i++ {
      comparison = ComparePackets(leftList[i], rightList[i])
      if comparison != 0 {
        return comparison
      }
    }

    return len(leftList) - len(rightList)
  } else if IsList(left) {
    newRight := fmt.Sprintf("[%s]", right)
    return ComparePackets(left, newRight)
  } else if IsList(right) {
    newLeft := fmt.Sprintf("[%s]", left)
    return ComparePackets(newLeft, right)
  } else {
    var lval, rval int
    fmt.Sscanf(left, "%d", &lval)
    fmt.Sscanf(right, "%d", &rval)
    return lval - rval
  }
}
