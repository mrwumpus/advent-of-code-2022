package day13

import (
  "bufio"
  "sort"
  "testing"
  "aoc/util/test"
)

func TestExample(t *testing.T) {

  examples := []struct{
    left, right string
    inOrder bool
  } {
    { "[1,1,3,1,1]", "[1,1,5,1,1]", true },
    { "[[1],[2,3,4]]", "[[1],4]", true },
    { "[9]", "[[8,7,6]]", false },
    { "[[4,4],4,4]", "[[4,4],4,4,4]", true },
    { "[7,7,7,7]", "[7,7,7]", false },
    { "[]", "[3]", true },
    { "[[[]]]", "[[]]", false },
    { "[1,[2,[3,[4,[5,6,7]]]],8,9]", "[1,[2,[3,[4,[5,6,0]]]],8,9]", false },
  }

  for _, example := range examples {
    cmp := ComparePackets(example.left, example.right)

    if cmp < 0 != example.inOrder {
      t.Errorf("%s and %s should be %v", example.left, example.right, example.inOrder)
    }
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    pairs := ParsePairs(scanner)

    inOrderIndexes := []int{}
    indexSum := 0

    for i, pair := range pairs {
      left := (*pair)[0]
      right := (*pair)[1]

      if ComparePackets(left, right) < 0 {
        inOrderIndexes = append(inOrderIndexes, i+1)
        indexSum+=i+1
      }
    }

    if indexSum <= 5326 {
      t.Errorf("Sum should be GREATER THAN 5326, but we got %d", indexSum)
    } else {

      expectedSum := 5529
      if indexSum != expectedSum {
        t.Errorf("Expected sum %d, but got %d", expectedSum, indexSum)
      }
    }

  })
}

func findPacketIdx(packet string, packets []string) int {
  for i, p := range packets {
    if p == packet {
      return i
    }
  }

  return -1
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    dividerOne := "[[2]]"
    dividerTwo := "[[6]]"

    packets := []string{dividerOne, dividerTwo}

    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        packets = append(packets, txt)
      }
    }

    sort.Slice(packets, func(i, j int) bool {
      left := packets[i]
      right := packets[j]
      return ComparePackets(left, right) < 0
    })

    idxOne := findPacketIdx(dividerOne, packets) + 1
    idxTwo := findPacketIdx(dividerTwo, packets) + 1

    key := idxOne * idxTwo

    expectedKey := 27690
    if key != expectedKey {
      t.Errorf("Expected key to be %d, but got %d", expectedKey, key)
    }

  })
}
