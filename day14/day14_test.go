package day14

import (
  "bufio"
  "testing"
  //"aoc/util/graph"
  "aoc/util/test"
)

var exampleLines = []string{
  "498,4 -> 498,6 -> 496,6",
  "503,4 -> 502,4 -> 502,9 -> 494,9",
}

func max(a int, b int) int {
  if a > b {
    return a
  }
  return b
}

func TestExample(t *testing.T) {
  walls := Line{}
  for _, str := range exampleLines {
    for _, p := range ParseLine(str) {
      walls = append(walls, p)
    }
  }

  sand := DropSand(walls)

  //graph.PrintPoints(walls, "#", sand, "o")

  grainCount := len(sand)
  if grainCount != 24 {
    t.Fail()
  }

  sand = DropSandToBottom(walls)
  //graph.PrintPoints(walls, "#", sand, "o")
  if len(sand) != 93 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    walls := Line{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        for _, p := range ParseLine(txt) {
          walls = append(walls, p)
        }
      }
    }

    sand := DropSand(walls)

    //graph.PrintPoints(walls, "#", sand, "o")
    grainCount := len(sand)
    expectedCount := 885

    if grainCount != expectedCount {
      t.Errorf("Expected %d grains of sand to stack up, but counted %d", expectedCount, grainCount)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    walls := Line{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        for _, p := range ParseLine(txt) {
          walls = append(walls, p)
        }
      }
    }

    sand := DropSandToBottom(walls)

    //graph.PrintPoints(walls, "#", sand, "o")
    grainCount := len(sand)
    expectedCount := 28691

    if grainCount != expectedCount {
      t.Errorf("Expected %d grains of sand to stack up, but counted %d", expectedCount, grainCount)
    }
  })
}
