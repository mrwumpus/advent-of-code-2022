package day14

import (
  "fmt"
  "strings"
  "aoc/util/graph"
)

type Line []*graph.Point

func strtoi(input string) (val int) {
  fmt.Sscanf(input, "%d", &val)
  return
}

func ParseLine(input string) Line {
  points := []*graph.Point{}
  pairs := strings.Split(input, " -> ")

  var x,y int
  var start *graph.Point
  for _, pair := range pairs {
    split := strings.Split(pair, ",")
    x, y = strtoi(split[0]), strtoi(split[1])

    p := graph.NewPoint(x,y)
    if start != nil {
      for _, pt := range graph.LinePoints(start, p) {
        points = append(points, pt)
      }
    }

    start = p
  }

  return points
}

func dropSandStep(start *graph.Point, occupied map[string]bool, maxY int) *graph.Point {
  nextDown := start.Neighbor(graph.Down)

  next := nextDown

  if occupied[next.Print()] {
    next = nextDown.Neighbor(graph.Left)
    if occupied[next.Print()] {
      next = nextDown.Neighbor(graph.Right)
      if occupied[next.Print()] {
        next = start
      }
    }
  }

  if next.Below(maxY) {
    return nil
  }

  return next
}

func DropSandGrain(start *graph.Point, occupied map[string]bool, maxY int) (bool, *graph.Point) {
  next := start

  var current *graph.Point
  for next != nil && !next.Same(current) {
    current = next
    next = dropSandStep(current, occupied, maxY)
  }

  if next == nil {
    return true, current
  }

  return false, next
}

func DropSand(walls Line) Line {
  occupied := map[string]bool{}
  var maxY int
  for _, p := range walls {
    occupied[p.Print()] = true
    maxY = max(maxY, p.Y())
  }

  dropPoint := graph.NewPoint(500, 0)
  sand := []*graph.Point{}

  hitBottom, grain := DropSandGrain(dropPoint, occupied, maxY)
  for !hitBottom {
    sand = append(sand, grain)
    occupied[grain.Print()] = true

    hitBottom, grain = DropSandGrain(dropPoint, occupied, maxY)
  }

  return sand
}

func DropSandToBottom(walls Line) Line {
  occupied := map[string]bool{}
  var maxY int
  for _, p := range walls {
    occupied[p.Print()] = true
    maxY = max(maxY, p.Y())
  }

  maxY += 2

  dropPoint := graph.NewPoint(500, 0)
  sand := []*graph.Point{}

  _, grain := DropSandGrain(dropPoint, occupied, maxY)
  for !grain.Same(dropPoint) {
    sand = append(sand, grain)
    occupied[grain.Print()] = true

    _, grain = DropSandGrain(dropPoint, occupied, maxY)
  }

  sand = append(sand, dropPoint)
  return sand
}
