package day6

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

func TestExample(t *testing.T) {
  cases := []struct {
    buffer string
    expectedIdx int
  } {
    { "bvwbjplbgvbhsrlpgdmjqwftvncz", 5 },
    { "nppdvjthqldpwncqszvftbrmjlhg", 6 },
    { "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10 },
    { "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11 },
  }

  for _, testcase := range cases {
    markerIdx := FindFirstMarker(testcase.buffer, 4)

    if markerIdx != testcase.expectedIdx {
      t.Errorf("Expected idx: %d, but indtead got %d", testcase.expectedIdx, markerIdx)
    }
  }
}

func findWithLen(scanner *bufio.Scanner, length int) (markerIdx int) {
  for scanner.Scan() {
    txt := scanner.Text()

    if txt != "" {
      markerIdx = FindFirstMarker(txt, length)
    }
  }

  return
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    markerIdx := findWithLen(scanner, 4)

    expected := 1723
    if markerIdx != expected {
      t.Errorf("Expected to find marker index %d, but instead came up with %d", expected, markerIdx)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    markerIdx := findWithLen(scanner, 14)

    expected := 3708
    if markerIdx != expected {
      t.Errorf("Expected to find marker index %d, but instead came up with %d", expected, markerIdx)
    }
  })
}
