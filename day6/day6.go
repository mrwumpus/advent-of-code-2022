package day6

func addMarkerRune(hash *map[rune]int, ch rune) {
  count, exists := (*hash)[ch]
  if !exists {
    count = 1
  } else {
    count += 1
  }
  (*hash)[ch] = count
}

func removeMarkerRune(hash *map[rune]int, ch rune) {
  count, exists := (*hash)[ch]
  if exists {
    count -= 1
    if count == 0 {
      delete(*hash, ch)
    } else {
      (*hash)[ch] = count
    }
  }
}

func FindFirstMarker(chars string, uniqueLen int) int {
  packet := []rune{}
  markerHash := map[rune]int{}

  for i, ch := range chars {
    packet = append(packet, ch)
    addMarkerRune(&markerHash, ch)

    if len(packet) > uniqueLen {
      first := packet[0]
      packet = packet[1:]
      removeMarkerRune(&markerHash, first)
    }

    if len(markerHash) == uniqueLen {
      return i + 1
    }
  }

  return -1
}
