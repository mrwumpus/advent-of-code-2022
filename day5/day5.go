package day5

import (
  "fmt"
  "aoc/util/stack"
)

type Pallets []*stack.Stack[rune]

func getRune(str string, idx int) rune {
  for i, ch := range str {
    if i == idx {
      return ch
    }
  }

  return ' '
}

func ParseStacks(lines []string) (stacks Pallets) {
  base, crates := lines[len(lines)-1], lines[:len(lines)-1]

  stacks = Pallets{}

  for i, ch := range base {
    if ch != ' ' {
      var mystack *stack.Stack[rune]
      for _, crate := range crates {
        item := getRune(crate, i)
        if item != ' ' {
          if mystack == nil {
            mystack = stack.New(item)
          } else {
            mystack.Push(item)
          }
        }
      }

      stacks = append(stacks, mystack.Reverse())
    }
  }

  return
}

func ParseDirection(directionStr string) (source, dest, amt int, err error) {
  _, err = fmt.Sscanf(directionStr, "move %d from %d to %d", &amt, &source, &dest)
  return
}

func ExecuteDirection(stacks Pallets, directionStr string) {
  source, dest, amt, err := ParseDirection(directionStr)
  if err != nil {
    panic(err)
  }

  sourceStack, destStack := stacks[source - 1], stacks[dest - 1]

  for i := 0; i < amt; i++ {
    destStack.Push(sourceStack.Pop())
  }
}

func ExecuteDirectionForCrateMover9001(stacks Pallets, directionStr string) {
  source, dest, amt, err := ParseDirection(directionStr)
  if err != nil {
    panic(err)
  }

  if amt == 0 {
    return
  }

  sourceStack, destStack := stacks[source - 1], stacks[dest - 1]

  tmp := stack.New(sourceStack.Pop())

  for i := 1; i < amt; i++ {
    tmp.Push(sourceStack.Pop())
  }

  for !tmp.IsEmpty() {
    destStack.Push(tmp.Pop())
  }
}

func ReadTops(stacks Pallets) (tops []rune) {
  tops = []rune{}

  for _, curStack := range stacks {
    tops = append(tops, curStack.Peek('_'))
  }

  return
}
