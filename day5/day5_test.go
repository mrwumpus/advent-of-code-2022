package day5

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

var stacksStr = []string{
  "    [D]    ",
  "[N] [C]    ",
  "[Z] [M] [P]",
  " 1   2   3 ",
}

func TestExampleCratesParsing(t *testing.T) {
  stacks := ParseStacks(stacksStr)

  if len(stacks) != 3 {
    t.Errorf("Expected 3 stacks, but parsed %d", len(stacks))
  }

  stack := stacks[0]
  N, Z := stack.Pop(), stack.Pop()

  if N != 'N' || Z != 'Z' {
    t.Errorf("%q, %q", N, Z)
  }

  if !stack.IsEmpty() {
    t.Errorf("Stack should be empty but isn't")
  }

  stack = stacks[1]
  D, C, M := stack.Pop(), stack.Pop(), stack.Pop()

  if D != 'D' || C != 'C' || M != 'M' {
    t.Errorf("%q, %q, %q", D, C, M)
  }

  if !stack.IsEmpty() {
    t.Errorf("Stack should be empty but isn't")
  }

  stack = stacks[2]
  P := stack.Pop()

  if P != 'P' {
    t.Errorf("%q", P)
  }

  if !stack.IsEmpty() {
    t.Errorf("Stack should be empty but isn't")
  }
}

func TestExample(t *testing.T) {
  stacks := ParseStacks(stacksStr)

  directions := []string{
    "move 1 from 2 to 1",
    "move 3 from 1 to 3",
    "move 2 from 2 to 1",
    "move 1 from 1 to 2",
  }

  for _,direction := range directions {
    ExecuteDirection(stacks, direction)
  }

  tops := string(ReadTops(stacks))

  if tops != "CMZ" {
    t.Errorf("Expected CMZ but got %q", tops)
  }
}

func executeWithFunction(scanner *bufio.Scanner, fn func(Pallets, string)) string {
    inputStacks := []string{}
    var stacks Pallets

    for scanner.Scan() {
      txt := scanner.Text()

      if len(stacks) > 0 {
        fn(stacks, txt)
      } else {
        if txt == "" {
          stacks = ParseStacks(inputStacks)
        } else {
          inputStacks = append(inputStacks, txt)
        }
      }
    }

    return string(ReadTops(stacks))
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {

    tops := executeWithFunction(scanner, ExecuteDirection)
    expected := "FZCMJCRHZ"

    if tops != expected {
      t.Errorf("Tops should be %q, but got %q", expected, tops)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {

    tops := executeWithFunction(scanner, ExecuteDirectionForCrateMover9001)
    expected := "JSDHQMZGF"

    if tops != expected {
      t.Errorf("Tops should be %q, but got %q", expected, tops)
    }
  })
}
