package day17

import (
  "fmt"
  "strings"
  "aoc/util"
  "aoc/util/graph"
)

const (
  Flat = "####"
  Plus = `.#.
###
.#.`
  Ell = `..#
..#
###`
  Stack = `#
#
#
#`
  Block = `##
##`
)

type Formation struct {
  shapeStr string
  minp, maxp, p *graph.Point
  shape []*graph.Point
}

func formationGenerator() func() string {
  shapes := []string{
    Flat, Plus, Ell, Stack, Block,
  }

  var shapeIdx int

  return func() (shape string) {
    shape = shapes[shapeIdx]
    shapeIdx = util.Mod(shapeIdx + 1, 5)
    return
  }
}

func directionGenerator(dirs string) func() graph.Direction {
  var dirIdx int

  return func () (dir graph.Direction) {
    next := dirs[dirIdx]

    dir = graph.Right

    if next == '<' {
      dir = graph.Left
    }

    dirIdx = util.Mod(dirIdx + 1, len(dirs))
    return
  }
}

func Path(s string, p *graph.Point) []*graph.Point {
  pts := []*graph.Point{}
  currentx := p
  currenty := p
  for _, str := range strings.Split(s, "\n") {
    for _, ch := range str {
      if ch == '#' {
        pts = append(pts, currentx)
      }
      currentx = currentx.Neighbor(graph.Right)
    }
    currenty = currenty.Neighbor(graph.Down)
    currentx = currenty
  }

  return pts
}

func MakeFormation(shape string, starty int) *Formation {
  height := len(strings.Split(shape, "\n"))
  start := graph.NewPoint(2, starty - height)
  f := Formation{ shapeStr: shape,
                  p: start }

  f.CalcAfterPointChange()
  return &f
}

func (f *Formation) Neighbor(dir graph.Direction) *Formation {
  nextPoint := f.p.Neighbor(dir)
  newF := Formation{ shapeStr: f.shapeStr,
                     p: nextPoint }

  newF.CalcAfterPointChange()
  return &newF
}

func (f *Formation) CalcAfterPointChange() {
  f.shape = Path(f.shapeStr, f.p)
  minp, maxp := graph.Bounds(f.shape)
  f.minp, f.maxp = minp, maxp
}

func (f *Formation) InBounds(maxX int) bool {
  return f.minp.X() >= 0 && f.maxp.X() < maxX
}

func (f *Formation) Intersect(floor map[string]bool) bool {
  for _, p := range f.shape {
    if floor[p.Print()] {
      return true
    }
  }

  return false
}

func (f *Formation)Valid(floor map[string]bool, maxX int) bool {
  return f.InBounds(maxX) && !f.Intersect(floor)
}

func (f *Formation)Print() {
  graph.PrintPath(f.shape)
}

func DropNextShape(shapefn func() string, windfn func() graph.Direction, floor map[string]bool, starty int, maxX int) (int, *Formation) {
  shape := shapefn()
  f := MakeFormation(shape, starty)
  
  var steps int

  for {
    steps++
    // Blow it
    dir := windfn()
    moved := f.Neighbor(dir)
    if moved.Valid(floor, maxX) {
      f = moved
    }

    // Now drop it
    moved = f.Neighbor(graph.Down)
    if moved.Valid(floor, maxX) {
      f = moved
    } else {
      return steps, f
    }
  }
}

func PrintFormations(f *Formation, fs []*Formation) {
  ps := []*graph.Point{graph.NewPoint(0,0)}
  for _, f := range fs {
    ps = append(ps, f.shape...)
  }

  graph.PrintPoints(ps, "#", f.shape, "@")
}

func dropLeftRight(p *graph.Point, floor map[string]bool, newFloor, visited *map[string]bool, f *Formation, maxX int) {
  if (*visited)[p.Print()] {
    return
  }

  if floor[p.Print()] || p.InPath(f.shape) {
    (*newFloor)[p.Print()] = true
    return
  }

  (*visited)[p.Print()] = true
  next := p.Neighbor(graph.Down)
  dropLeftRight(next, floor, newFloor, visited, f, maxX)

  next = p.Neighbor(graph.Left)
  if next.X() >= 0 && !(*visited)[next.Print()] {
    dropLeftRight(next, floor, newFloor, visited, f, maxX)
  }

  next = p.Neighbor(graph.Right)
  if next.X() < maxX && !(*visited)[next.Print()] {
    dropLeftRight(next, floor, newFloor, visited, f, maxX)
  }
}

func mergeFloor(floor map[string]bool, f *Formation, startY, maxX int) map[string] bool {
  visited := map[string]bool{}
  newFloor := map[string]bool{}

  for x := 0; x < maxX; x++ {
    start := graph.NewPoint(x, startY)
    if !visited[start.Print()] {
      dropLeftRight(start, floor, &newFloor, &visited, f, maxX)
    }
  }

  return newFloor
}

func scoreFloor(floor map[string]bool) string {
  var x,y int

  minArry := make([]int, 7)

  for k, v := range floor {
    if v {
      fmt.Sscanf(k, "[%d,%d]", &x, &y)
      minArry[x] = util.Min(minArry[x], y)
    }
  }

  val := minArry[0]
  minArry[0] = 0
  for x = 1; x < len(minArry); x++ {
    next := minArry[x]
    diff := val - next
    minArry[x] = diff
    val = next
  }

  return fmt.Sprintf("%d|%d|%d|%d|%d|%d",
                      minArry[1],
                      minArry[2],
                      minArry[3],
                      minArry[4],
                      minArry[5],
                      minArry[6])
}

func DropShapes(windStr string, shapeDrops int) int {
  maxX, startY := 7, -3
  var curY int

  floor := map[string]bool{}
  for i := 0; i < maxX; i++ {
    floor[graph.NewPoint(i, 0).Print()] = true
  }

  shapes := formationGenerator()
  winds := directionGenerator(windStr)

  var windCycle, shapeCycle int
  windDiv, shapeDiv := len(windStr), 5
  prevCycles := map[string]int{}
  prevHeight := map[string]int{}

  var hitCycle bool
  var cycleAddition int

  for i := 0; i < shapeDrops; i++ {
    cycleName := fmt.Sprintf("%s|%d,%d", scoreFloor(floor), windCycle, shapeCycle)

    setupSteps := prevCycles[cycleName]
    if setupSteps > 0 && cycleAddition == 0 && !hitCycle {
      hitCycle = true
      //fmt.Printf("%d: %s from %d -- %v\n", i, cycleName, prevCycles[cycleName], floor)

      cycleLength := i - setupSteps
      remaining := shapeDrops - setupSteps - cycleLength
      cycleCount := remaining / cycleLength
      remainder := remaining - (cycleCount*cycleLength)
      cycleHeight := curY - prevHeight[cycleName]

      /*
      println(remaining, "drops remaining")
      println(cycleLength, "cycle length")
      println("Needs", cycleCount, "more cycles")
      println("and then", remainder, "more")
      println("Each cycle is", cycleHeight)
      */

      cycleAddition = cycleCount * cycleHeight

      i = shapeDrops - remainder
    }

    prevCycles[cycleName] = i
    prevHeight[cycleName] = curY

    steps, f := DropNextShape(shapes, winds, floor, curY + startY, maxX)

    curY = util.Min(curY, f.minp.Y())

    //PrintFormations(f, formations)

    floor = mergeFloor(floor, f, curY, maxX)

    shapeCycle = util.Mod(shapeCycle + 1, shapeDiv)
    windCycle = util.Mod(windCycle + steps, windDiv)
  }

  return -(curY + cycleAddition)
}
