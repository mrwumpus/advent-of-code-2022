package day17

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

var exampleWinds = ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>"

func TestExample(t *testing.T) {
  height := DropShapes(exampleWinds, 2022)

  if height != 3068 {
    t.Errorf("Should be 3068, but its %d", height)
  }

  // Need to locate a cycle!
  height = DropShapes(exampleWinds, 1000000000000)

  expected := 1514285714288
  if height != expected {
    t.Errorf("Should be %d, but its %d", expected, height)
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    var weatherString string
    for scanner.Scan() {
      weatherString = scanner.Text()
    }

    height := DropShapes(weatherString, 2022)

    expectedHeight := 3149
    if height != expectedHeight {
      t.Errorf("Should be %d, but its %d", expectedHeight, height)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    var weatherString string
    for scanner.Scan() {
      weatherString = scanner.Text()
    }

    height := DropShapes(weatherString, 1000000000000)

    expectedHeight := 1553982300884
    if height != expectedHeight {
      t.Errorf("Should be %d, but its %d", expectedHeight, height)
    }
  })
}
