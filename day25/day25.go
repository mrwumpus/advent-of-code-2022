package day25

import (
  "fmt"
  "math"
  "strings"
)

func ReadSNAFU(num string) (value int) {
  var pow, place int
  pow = 1
  for i := len(num) - 1; i >= 0; i-- {
    ch := num[i]
    if ch == '=' {
      place = -2
    } else if ch == '-' {
      place = -1
    } else {
      place = int(ch - '0')
    }

    value += place * pow
    pow*=5
  }

  return
}

func WriteSNAFU(num int) string {
  var pow int
  remaining := num
  pow = 0
  snafu := ""
  for math.Pow(5, float64(pow)) < float64(num) {
    pow++
  }

  numbers := make([]int, pow+1)

  for pow >= 0 {
    place := int(math.Pow(5, float64(pow)))
    val := remaining / place
    remaining = int(math.Mod(float64(remaining), float64(place)))
    numbers[pow] = val
    pow--
  }

  for i := 0; i < len(numbers); i++ {
    num := numbers[i]
    if num == 5 {
      num = 0
      numbers[i+1] = numbers[i+1]+1
    }

    if num < 3 {
      snafu = fmt.Sprintf("%d%s", num, snafu)
    } else if num == 3 {
      snafu = fmt.Sprintf("%s%s", "=", snafu)
      numbers[i+1] = numbers[i+1]+1
    } else {
      snafu = fmt.Sprintf("%s%s", "-", snafu)
      numbers[i+1] = numbers[i+1]+1
    }
  }

  return strings.TrimPrefix(snafu, "0")
}
