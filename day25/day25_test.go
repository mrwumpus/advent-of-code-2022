package day25

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

var examples = []struct{
  snafu string
  num int
}{
  {"1=-0-2", 1747},
  {"12111",  906},
  {"2=0=",   198},
  {"21",     11},
  {"2=01",   201},
  {"111",    31},
  {"20012",  1257},
  {"112",    32},
  {"1=-1=",  353},
  {"1-12",   107},
  {"12",     7},
  {"1=",     3},
  {"122",    37},
}

func TestExample(t *testing.T) {
  for _, example := range examples {
    result := ReadSNAFU(example.snafu)
    if result != example.num {
      t.Errorf("SNAFU Num %s should be %d, but was read as %d", example.snafu, example.num, result)
    }

    if WriteSNAFU(result) != example.snafu {
      t.Fail()
    }
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    var total int
    for scanner.Scan() {
      txt := scanner.Text()

      total += ReadSNAFU(txt)
    }

    totalSnafu := WriteSNAFU(total)
    expected := "2=1-=02-21===-21=200"
    if totalSnafu != expected {
      t.Errorf("Expected to get %s, but instead got %s", expected, totalSnafu)
    }
  })
}
