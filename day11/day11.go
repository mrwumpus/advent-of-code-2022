package day11

import (
  "fmt"
  "math"
  "sort"
  "strings"
)

type Monkey struct {
  items []int
  factors []*map[int]float64
  // Function that takes in an item worry and returns a new one.
  operation func(int) int
  // Function that takes in the item worry, applies a test function and
  // returns the target number of the monkey to toss to.
  test func(int) int
  testValue, trueMonkey, falseMonkey, inspectionCount int
}

func initFactors(monkeys *[]*Monkey) {
  for _ , monkey := range *monkeys {
    for _, factMonk := range *monkeys {
      factMonk.addFactor(monkey.testValue)
    }
  }
}

func (monkey *Monkey) addFactor(div int) {
  for i, item := range monkey.items {
    factor := monkey.factors[i]
    val := math.Mod(float64(item), float64(div))
    (*factor)[div] = val
    //println("setting monkey factor", div, "using initial item", item, "to", int(val))
  }
}

func (monkey *Monkey) Print(i int) {
  fmt.Printf("Monkey %d: ", i)
  for j, item := range monkey.items {
    if j > 0 {
      print(", ")
    }
    print(item)
  }
  println()
  for _, factor := range monkey.factors {
    for k, v := range *factor {
      fmt.Printf("[%d] %v, ", k, v)
    }
    println()
  }

  println()
  fmt.Printf("  inspected items %d times.\n", monkey.inspectionCount)
}

func (monkey *Monkey) inspect() int {
  if len(monkey.items) > 0 {
    item := monkey.items[0]
    monkey.items = monkey.items[1:]
    monkey.inspectionCount++

    return monkey.operation(item)
  }

  return -1
}

func (monkey *Monkey) inspectFactor() *map[int]float64 {
  if len(monkey.factors) > 0 {
    factor := monkey.factors[0]
    monkey.factors = monkey.factors[1:]
    monkey.inspectionCount++

    newMap := map[int]float64{}

    for k, v := range *factor {
      newValue := math.Mod(float64(monkey.operation(int(v))), float64(k))
      newMap[k] = newValue
    }

    return &newMap
  }

  return nil
}

func (monkey *Monkey) catch(item int) {
  monkey.items = append(monkey.items, item)
}

func (monkey *Monkey) catchFactor(factor *map[int]float64) {
  monkey.factors = append(monkey.factors, factor)
}

func (monkey *Monkey) takeTurn(monkeys *[]*Monkey) {
  for len(monkey.items) > 0 {
    item := monkey.inspect()

    item /= 3

    monkeyTarget := monkey.test(item)
    (*monkeys)[monkeyTarget].catch(item)
  }
}

func (monkey *Monkey) factorTurn(monkeys *[]*Monkey) {
  for len(monkey.factors) > 0 {
    factor := monkey.inspectFactor()

    var monkeyTarget *Monkey

    if (*factor)[monkey.testValue] == 0 {
      monkeyTarget = (*monkeys)[monkey.trueMonkey]
    } else {
      monkeyTarget = (*monkeys)[monkey.falseMonkey]
    }

    monkeyTarget.catchFactor(factor)
  }
}

func RunRounds(monkeys *[]*Monkey, num int, withRelief bool) {
  if !withRelief {
    initFactors(monkeys)
  }

  for i := 0; i < num; i++ {
    for _, monkey := range *monkeys {
      if withRelief {
        monkey.takeTurn(monkeys)
      } else {
        monkey.factorTurn(monkeys)
      }
    }
  }
}

func MonkeyBusiness(monkeys *[]*Monkey) int {
  counts := []int{}
  for _, monkey := range *monkeys {
    counts = append(counts, monkey.inspectionCount)
  }

  sort.Ints(counts)
  lastTwo := counts[len(counts)-2:]

  first := lastTwo[0]
  second := lastTwo[1]

  return first * second
}

func InputParser() (*[]*Monkey, func(string)) {
  monkeys := &[]*Monkey{}
  var currentMonkey *Monkey

  var testValue, trueMonkey int

  return monkeys, func(inputLine string) {
    line := strings.TrimSpace(inputLine)

    if strings.HasPrefix(line, "Monkey") {
      currentMonkey = &Monkey{}
      *monkeys = append(*monkeys, currentMonkey)
      return
    }

    if strings.HasPrefix(line, "Starting items:") {
      currentMonkey.items = parseStartingItems(line)
      factors := make([]*map[int]float64, len(currentMonkey.items))
      for i := 0; i < len(factors); i++ {
        factorMap := make(map[int]float64)
        factors[i] = &factorMap
      }
      currentMonkey.factors = factors
      return
    }

    if strings.HasPrefix(line, "Operation:") {
      currentMonkey.operation = parseOperation(line)
      return
    }

    if strings.HasPrefix(line, "Test:") {
      fmt.Sscanf(line, "Test: divisible by %d", &testValue)
      return
    }

    if strings.HasPrefix(line, "If true") {
      fmt.Sscanf(line, "If true: throw to monkey %d", &trueMonkey)
      currentMonkey.trueMonkey = trueMonkey
      return
    }

    if strings.HasPrefix(line, "If false") {
      var falseMonkey int
      fmt.Sscanf(line, "If false: throw to monkey %d", &falseMonkey)

      currentMonkey.test = parseTest(testValue, trueMonkey, falseMonkey)
      currentMonkey.testValue = testValue
      currentMonkey.falseMonkey = falseMonkey
    }
  }
}

func parseStartingItems(line string) (items []int) {
  itemsStr := strings.TrimPrefix(line, "Starting items: ")
  worryList := strings.Split(itemsStr, ", ")

  items = []int{}
  var itemNum int
  for _, item := range worryList {
    fmt.Sscanf(item, "%d", &itemNum)
    items = append(items, itemNum)
  }

  return
}

func parseOperation(line string) func(int) int {
  var op, operand string

  fmt.Sscanf(line, "Operation: new = old %s %s", &op, &operand)

  switch op {
  case "*":
    if operand == "old" {
      return func(input int) int { return input * input }
    } else {
      var val int
      fmt.Sscanf(operand, "%d", &val)
      return func(input int) int { return input * val }
    }
  case "+":
    if operand == "old" {
      return func(input int) int { return input + input }
    } else {
      var val int
      fmt.Sscanf(operand, "%d", &val)
      return func(input int) int { return input + val }
    }
  }

  return func(input int) int { return input }
}

func parseTest(testValue int, trueMonkey int, falseMonkey int) func(int) int {
  return func(input int) int {
    if math.Mod(float64(input), float64(testValue)) == 0 {
      return trueMonkey
    }

    return falseMonkey
  }
}
