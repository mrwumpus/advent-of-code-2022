package day11

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

var testInput = `Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1`

func TestExample(t *testing.T) {
  monkeys, parser := InputParser()

  for _, line := range strings.Split(testInput, "\n") {
    parser(line)
  }

  RunRounds(monkeys, 20, true)

  //for i, monkey := range *monkeys {
    //monkey.Print(i)
  //}

  mb := MonkeyBusiness(monkeys)

  if mb != 10605 {
    t.Fail()
  }
}

func TestExampleTwo(t *testing.T) {
  monkeys, parser := InputParser()

  for _, line := range strings.Split(testInput, "\n") {
    parser(line)
  }

  RunRounds(monkeys, 10000, false)

  mb := MonkeyBusiness(monkeys)

  if mb != 2713310158 {
    t.Fail()
  }
}


func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    monkeys, parser := InputParser()
    for scanner.Scan() {
      txt := scanner.Text()
      parser(txt)
    }

    RunRounds(monkeys, 20, true)
    mb := MonkeyBusiness(monkeys)

    expectedMonkeyBusiness := 100345
    if mb != expectedMonkeyBusiness {
      t.Errorf("Expected this much monkey business: %d, but instead got %d", expectedMonkeyBusiness, mb)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    monkeys, parser := InputParser()
    for scanner.Scan() {
      txt := scanner.Text()
      parser(txt)
    }

    RunRounds(monkeys, 10000, false)
    mb := MonkeyBusiness(monkeys)

    expectedMonkeyBusiness := 28537348205
    if mb != expectedMonkeyBusiness {
      t.Errorf("Expected this much monkey business: %d, but instead got %d", expectedMonkeyBusiness, mb)
    }
  })
}
