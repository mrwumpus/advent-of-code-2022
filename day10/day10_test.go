package day10

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

var testInstructions = []string {
  "noop",
  "addx 3",
  "addx -5",
}

var longInstructions = []string {
  "addx 15",
  "addx -11",
  "addx 6",
  "addx -3",
  "addx 5",
  "addx -1",
  "addx -8",
  "addx 13",
  "addx 4",
  "noop",
  "addx -1",
  "addx 5",
  "addx -1",
  "addx 5",
  "addx -1",
  "addx 5",
  "addx -1",
  "addx 5",
  "addx -1",
  "addx -35",
  "addx 1",
  "addx 24",
  "addx -19",
  "addx 1",
  "addx 16",
  "addx -11",
  "noop",
  "noop",
  "addx 21",
  "addx -15",
  "noop",
  "noop",
  "addx -3",
  "addx 9",
  "addx 1",
  "addx -3",
  "addx 8",
  "addx 1",
  "addx 5",
  "noop",
  "noop",
  "noop",
  "noop",
  "noop",
  "addx -36",
  "noop",
  "addx 1",
  "addx 7",
  "noop",
  "noop",
  "noop",
  "addx 2",
  "addx 6",
  "noop",
  "noop",
  "noop",
  "noop",
  "noop",
  "addx 1",
  "noop",
  "noop",
  "addx 7",
  "addx 1",
  "noop",
  "addx -13",
  "addx 13",
  "addx 7",
  "noop",
  "addx 1",
  "addx -33",
  "noop",
  "noop",
  "noop",
  "addx 2",
  "noop",
  "noop",
  "noop",
  "addx 8",
  "noop",
  "addx -1",
  "addx 2",
  "addx 1",
  "noop",
  "addx 17",
  "addx -9",
  "addx 1",
  "addx 1",
  "addx -3",
  "addx 11",
  "noop",
  "noop",
  "addx 1",
  "noop",
  "addx 1",
  "noop",
  "noop",
  "addx -13",
  "addx -19",
  "addx 1",
  "addx 3",
  "addx 26",
  "addx -30",
  "addx 12",
  "addx -1",
  "addx 3",
  "addx 1",
  "noop",
  "noop",
  "noop",
  "addx -9",
  "addx 18",
  "addx 1",
  "addx 2",
  "noop",
  "noop",
  "addx 9",
  "noop",
  "noop",
  "noop",
  "addx -1",
  "addx 2",
  "addx -37",
  "addx 1",
  "addx 3",
  "noop",
  "addx 15",
  "addx -21",
  "addx 22",
  "addx -6",
  "addx 1",
  "noop",
  "addx 2",
  "addx 1",
  "noop",
  "addx -10",
  "noop",
  "noop",
  "addx 20",
  "addx 1",
  "addx 2",
  "addx 2",
  "addx -6",
  "addx -11",
  "noop",
  "noop",
  "noop",
}

func contains(lst *[]int, checkVal int) bool {
  for _, val := range (*lst) {
    if val == checkVal {
      return true
    }
  }

  return false
}

func SumRegXAtCycles(instructions *[]string, sumCycles *[]int) int {
  stillRunning := false
  cpu, runCycle := InitInstructions(instructions)

  var sum int

  for {
    curValue := cpu.registerX

    stillRunning = runCycle()
    if !stillRunning {
      break
    }

    if contains(sumCycles, cpu.cycleCount) {
      sum += (cpu.cycleCount * curValue)
    }
  }

  return sum
}

func TestExample(t *testing.T) {
  stillRunning := false
  _, runCycle := InitInstructions(&testInstructions)

  for {
    stillRunning = runCycle()
    if !stillRunning {
      break
    }
    //println("Cycle:", cpu.cycleCount, "regX:", cpu.registerX)
  }

  sumCycles := []int{
    20, 60, 100, 140, 180, 220,
  }

  sum := SumRegXAtCycles(&longInstructions, &sumCycles)

  if sum != 13140 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    instructions := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        instructions = append(instructions, txt)
      }
    }

    sumCycles := []int{
      20, 60, 100, 140, 180, 220,
    }

    sum := SumRegXAtCycles(&instructions, &sumCycles)

    expectedSum := 14520

    if sum != expectedSum {
      t.Errorf("Sum should be %d, but got %d", expectedSum, sum)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    instructions := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        instructions = append(instructions, txt)
      }
    }

    cpu, runCycle := InitInstructions(&instructions)

    printMore := true
    for {
      printMore = PrintScanline(cpu, runCycle, 40)

      if ! printMore {
        break
      }
    }

  })
}
