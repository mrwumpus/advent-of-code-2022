package day10

import (
  "fmt"
  "strings"
)

type Instruction interface {
  cycle(cpu *Cpu) bool
}

type Cpu struct {
  cycleCount, registerX int
}

func (cpu *Cpu) Print() {
  println("Cycle:", cpu.cycleCount, "regX:", cpu.registerX)
}

func (cpu *Cpu) cycle() {
  cpu.cycleCount++
}

type Noop struct {}

func (*Noop) cycle(*Cpu) bool {
  return true
}

type AddX struct {
  cycleCount, adder int
}

func (inst *AddX) cycle(cpu *Cpu) bool {
  currentCycle := (*inst).cycleCount + 1
  if currentCycle == 2 {
    (*cpu).registerX += (*inst).adder
    return true
  }

  (*inst).cycleCount = currentCycle
  return false
}

func buildInstruction(instructionStr string) Instruction {
  if instructionStr == "noop" {
    return &Noop{}
  }

  if strings.HasPrefix(instructionStr, "addx") {
    var adder int
    fmt.Sscanf(instructionStr, "addx %d", &adder)
    return &AddX{0, adder}
  }

  return nil
}

func InitInstructions(instructions *[]string) (*Cpu, func() bool) {
  cpu := Cpu{0, 1}

  var instruction Instruction
  var instructionOffset int

  return &cpu, func() bool {
    if instruction == nil {
      if instructionOffset == len(*instructions) {
        return false
      }

      instruction = buildInstruction((*instructions)[instructionOffset])
      instructionOffset++
    }

    cpu.cycle()

    if instruction.cycle(&cpu) {
      instruction = nil
    }

    return true
  }
}

func PrintPixel(currentX int, currentPos int) {
  if currentPos >= currentX -1 && currentPos <= currentX + 1 {
    print("█")
  } else {
    print(".")
  }
}

func PrintScanline(cpu *Cpu, scanFunc func() bool, length int) bool {
  var currentX int
  for i := 0; i < length; i++ {
    currentX = cpu.registerX
    if ! scanFunc() {
      return false
    }

    PrintPixel(currentX, i)
  }
  println()

  return true
}


