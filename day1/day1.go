package main

import (
  "bufio"
  "fmt"
  "math"
  "os"
  "sort"
  "strconv"
)

func main() {
  f, err := os.Open("input")
  if err != nil {
    return
  }

  defer f.Close()

  part1(f)

  f.Seek(0, 0)
  part2(f)
}

func part1(f *os.File) {
  scanner := bufio.NewScanner(f)

  var total, max float64

  for scanner.Scan() {
    txt := scanner.Text()
    if txt == "" {
      max = math.Max(max, total)
      total = 0
    } else {
      val, _ := strconv.ParseFloat(txt, 64)
      total = total + val
    }
  }

  fmt.Printf("Max: %.f\n", max)
}

func part2(f *os.File) {
  scanner := bufio.NewScanner(f)

  var total float64
  top := []float64{}

  for scanner.Scan() {
    txt := scanner.Text()
    if txt == "" {
      top = determineTop(top, total)
      total = 0
    } else {
      val, _ := strconv.ParseFloat(txt, 64)
      total = total + val
    }
  }

  topTotal := sumTotal(top)

  fmt.Printf("Top 3 total: %.f\n", topTotal)
}

func determineTop(top []float64, total float64) []float64 {
  newTop := append(top, total)
  if len(newTop) == 4 {
    sort.Float64s(newTop)
    newTop = newTop[1:]
  }

  return newTop
}

func sumTotal(vals []float64) float64 {
  var total float64
  for _, ele := range vals {
    total += ele
  }

  return total
}
