package util

func Abs(a int) int {
  if a < 0 {
    return -a
  }
  return a
}

func Min(a int, b int) int {
  if a < b {
    return a
  }
  return b
}

func Max(a int, b int) int {
  if a > b {
    return a
  }
  return b
}

func Mod(a int, b int) int {
  div := a
  for div >= b {
    div -= b
  }

  return div
}
