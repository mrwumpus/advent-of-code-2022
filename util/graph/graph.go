package graph

import (
  "fmt"
  "strings"
)

type Point struct {
  x, y int
}

type Direction int

const (
  Up = iota
  Down
  Left
  Right
)

func NewPoint(x int, y int) *Point {
  return &Point{x,y}
}

func ParseDirection(dirStr string) Direction {
  switch dirStr {
  case "U":
    return Up
  case "D":
    return Down
  case "L":
    return Left
  default:
    return Right
  }
}

func ParseMovement(input string) (d Direction, steps int) {
  var dirStr string

  fmt.Sscanf(input, "%s %d", &dirStr, &steps)
  d = ParseDirection(dirStr)

  return
}

func (p *Point) Neighbor(dir Direction) *Point {
  switch dir {
  case Up:
    return &Point{p.x, p.y - 1}
  case Down:
    return &Point{p.x, p.y + 1}
  case Left:
    return &Point{p.x - 1, p.y}
  case Right:
    return &Point{p.x + 1, p.y}
  }

  return p
}

func (p *Point) Same(p2 *Point) bool {
  if p2 == nil {
    return p == nil
  }

  return p.x == p2.x && p.y == p2.y
}

func (p *Point) Below(maxY int) bool {
  return p.y >= maxY
}

func (p *Point) NextToOrSame(p2 *Point) bool {
  return p.x >= p2.x - 1 && p.x <= p2.x + 1 &&
         p.y >= p2.y - 1 && p.y <= p2.y + 1
}

func (p *Point) WithinDimensions(height int, width int) bool {
  return p.x >= 0 && p.x <= width && p.y >= 0 && p.y <= height
}

func (p *Point) Print() string {
  return fmt.Sprintf("[%d,%d]", p.x, p.y)
}

func (p *Point) Y() int {
  return p.y
}

func (p *Point) X() int {
  return p.x
}

func (p *Point) InPath(path []*Point) bool {
  for _, t := range path {
    if p.Same(t) {
      return true
    }
  }
  return false
}

func (p *Point) Distance(p2 *Point) int {
  return abs(p.x - p2.x) + abs(p.y - p2.y)
}

func (p *Point) WithinRange(p2 *Point, d int) bool {
  return p.Distance(p2) <= d
}

func abs(a int) int {
  if a < 0 {
    return -a
  }
  return a
}

func min(a int, b int) int {
  if a < b {
    return a
  }
  return b
}

func max(a int, b int) int {
  if a > b {
    return a
  }
  return b
}

func findPointAt(path []*Point, x int, y int) (int, *Point) {
  for i, p := range path {
    if p.x == x && p.y == y {
      return i, p
    }
  }
  return -1, nil
}

func LinePoints(p1 *Point, p2 *Point) []*Point {
  line := []*Point{p1, p2}
  if p1.x == p2.x {
    minY := min(p1.y, p2.y)
    maxY := max(p1.y, p2.y)

    for y := minY; y <= maxY; y++ {
      line = append(line, &Point{p1.x, y})
    }
  } else if p1.y == p2.y {
    minX := min(p1.x, p2.x)
    maxX := max(p1.x, p2.x)

    for x := minX; x <= maxX; x++ {
      line = append(line, &Point{x, p1.y})
    }
  }

  return line
}

func Bounds(path []*Point) (*Point, *Point) {
  if len(path) == 0 {
    return nil, nil
  }

  p := path[0]
  minX, minY := p.x, p.y
  maxX, maxY := p.x, p.y
  for i := 1; i < len(path); i++ {
    p = path[i]
    minX = min(minX, p.x)
    minY = min(minY, p.y)
    maxX = max(maxX, p.x)
    maxY = max(maxY, p.y)
  }

  return &Point{minX, minY}, &Point{maxX, maxY}
}

func (p *Point)InBounds(minp, maxp *Point) bool {
  return p.x >= minp.x && p.x <= maxp.x &&
         p.y >= minp.y && p.y <= maxp.y
}

func Intersect(path []*Point, other []*Point) *Point {
  minP, maxP := Bounds(path)
  minO, maxO := Bounds(other)

  return IntersectWithBounds(path, minP, maxP, other, minO, maxO)
}

func IntersectWithBounds(path []*Point, minP, maxP *Point, other []*Point, minO, maxO *Point) *Point {

  if (minP.y <= minO.y && minO.y <= maxP.y) ||
     (minP.y >= minO.y && minP.y <= maxO.y) {
    if (minP.x <= minO.x && minO.x <= maxP.x) ||
       (minP.x >= minO.x && minP.x <= maxO.x) {
         for _, p := range path {
           for _, o := range other {
             if p.Same(o) {
                return p
             }
           }
         }
     }
    }

  return nil
}

func PrintPoints(pts []*Point, char string, other []*Point, char2 string) {
  var p *Point
  var minX, minY, maxX, maxY int

  if len(pts) > 0 {
    p = pts[0]

    minX, minY = p.x, p.y
    maxX, maxY = p.x, p.y

    for i := 1; i < len(pts); i++ {
      p = pts[i]
      minX = min(minX, p.x)
      maxX = max(maxX, p.x)
      minY = min(minY, p.y)
      maxY = max(maxY, p.y)
    }
  }

  for i := 0; i < len(other); i++ {
    p = other[i]
    minX = min(minX, p.x)
    maxX = max(maxX, p.x)
    minY = min(minY, p.y)
    maxY = max(maxY, p.y)
  }

  for y := minY; y <= maxY; y++ {
    for x := minX; x <= maxX; x++ {
      _, p := findPointAt(pts, x, y)
      _, p2 := findPointAt(other, x, y)
      if p != nil && p2 != nil {
        print("x")
      } else if p != nil {
        print(char)
      } else if p2 != nil {
        print(char2)
      } else {
        print(".")
      }
    }
    println("")
  }
}

func PrintPath(path []*Point) {

  p := path[0]

  minX, minY := min(0, p.x), min(0, p.y)
  maxX, maxY := max(0, p.x), max(0, p.y)

  for i := 1; i < len(path); i++ {
    p = path[i]
    minX = min(minX, p.x)
    maxX = max(maxX, p.x)
    minY = min(minY, p.y)
    maxY = max(maxY, p.y)
  }

  for y := minY; y <= maxY; y++ {
    for x := minX; x <= maxX; x++ {
      i, p := findPointAt(path, x, y)
      if p == nil {
        if x == 0 && y == 0 {
          print("s")
        } else {
          print(".")
        }
      } else if i == 0 {
        print("E")
      } else {
        print("#")
      }
    }
    println("")
  }
}

func explodeString(str string, conversion func(string)int) []int {
  ints := []int{}

  var val int
  for _, char := range strings.Split(str, "") {
    val = conversion(char)
    ints = append(ints, val)
  }

  return ints
}

type IntMatrix [][]int

func NewMatrix(vals []string, conversion func(string)int) IntMatrix {
  newMatrix := make(IntMatrix, len(vals))
  for i, rowStr := range vals {
    newMatrix[i] = explodeString(rowStr, conversion)
  }

  return newMatrix
}

func (matrix IntMatrix) Dimensions() (height int, width int) {
  height = len(matrix)

  for _, row := range matrix {
    width = max(width, len(row))
  }

  return
}

func (matrix IntMatrix) findLocation(rowIdx int, colIdx int) *int {
  height := len(matrix)
  if rowIdx >= height || rowIdx < 0 {
    return nil
  }

  row := matrix[rowIdx]

  width := len(row)

  if colIdx >= width || colIdx < 0 {
    return nil
  }

  return &row[colIdx]
}

func (matrix IntMatrix) GetAtPoint(p *Point) int {
  return matrix.Get(p.y, p.x)
}

func (matrix IntMatrix) Get(rowIdx int, colIdx int) int {
  valLoc := matrix.findLocation(rowIdx, colIdx)
  if valLoc != nil {
    return *valLoc
  } else {
    return -1
  }
}

func (matrix IntMatrix) Print() {
  height, width := matrix.Dimensions()
  for i := 0; i < height; i++ {
    for j := 0; j < width; j++ {
      fmt.Printf(" %2d", matrix.Get(i, j))
    }
    println()
  }

}

