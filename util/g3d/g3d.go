package g3d

import (
  "fmt"
  "aoc/util"
)

type Direction int

const (
  Up Direction = iota
  Down
  Left
  Right
  In
  Out
)

//
//
// POINT
//
// a 3d point in space

type Point struct {
  x, y, z int
}


func NewPoint(x,y,z int) *Point {
  return &Point{x,y,z}
}

func ParsePoint(coords string) *Point {
  var x,y,z int

  fmt.Sscanf(coords, "%d,%d,%d", &x, &y, &z)

  return &Point{x,y,z}
}

func (p *Point)Neighbor(d Direction) *Point {
  x,y,z := p.x, p.y, p.z

  switch d {
  case Up:
    y++
  case Down:
    y--
  case Left:
    x--
  case Right:
    x++
  case In:
    z--
  case Out:
    z++
  }

  return &Point{x,y,z}
}

func (p *Point)Same(p2 *Point) bool {
  if p2 == nil {
    return false
  }

  return p.x == p2.x && p.y == p2.y && p.z == p2.z
}

func (p *Point)Str() string {
  return fmt.Sprintf("(%d,%d,%d)", p.x, p.y, p.z)
}

//
//
// BOUNDS
//
// a min and max point to define a region

type Bounds struct {
  minp, maxp *Point
}

func (b *Bounds)Expand(p *Point) {
  minX, minY, minZ := b.minp.x, b.minp.y, b.minp.z
  maxX, maxY, maxZ := b.maxp.x, b.maxp.y, b.maxp.z

  minX = util.Min(minX, p.x)
  maxX = util.Max(maxX, p.x)
  minY = util.Min(minY, p.y)
  maxY = util.Max(maxY, p.y)
  minZ = util.Min(minZ, p.z)
  maxZ = util.Max(maxZ, p.z)

  b.minp = &Point{minX, minY, minZ}
  b.maxp = &Point{maxX, maxY, maxZ}
}

func (b *Bounds)Grow(x,y,z int) *Bounds {
  minX, minY, minZ := b.minp.x, b.minp.y, b.minp.y
  maxX, maxY, maxZ := b.maxp.x, b.maxp.y, b.maxp.z

  return &Bounds{
    &Point{minX - z, minY - y, minZ - z},
    &Point{maxX + x, maxY + y, maxZ + z},
  }
}

func (b *Bounds)Contains(p *Point) bool {
  return b.minp.x <= p.x && b.minp.y <= p.y && b.minp.z <= p.z && b.maxp.x >= p.x && b.maxp.y >= p.y && b.maxp.z >= p.z
}

//
//
// POINTMAP
//
// a map of nested points for quick lookup

type PointMap struct {
  pmap map[int]*PointMap
  p *Point
}

func NewPointMap() *PointMap {
  return &PointMap{map[int]*PointMap{}, nil}
}

func (pm *PointMap)AddChild(idx int, p *Point) *PointMap {
  thisMap := pm.pmap

  childPm := thisMap[idx]
  if childPm == nil {
    childPm = NewPointMap()
    thisMap[idx] = childPm
  }

  return childPm
}

func (pm *PointMap)AddPoint(p *Point) {
  childPm := pm.AddChild(p.x, p)
  childPm = childPm.AddChild(p.y, p)
  childPm = childPm.AddChild(p.z, p)
  childPm.p = p
}

func (pm *PointMap)Find(x,y,z int) *Point {
  child := pm.pmap[x]
  if child == nil {
    return nil
  }

  child = child.pmap[y]
  if child == nil {
    return nil
  }

  child = child.pmap[z]
  if child == nil {
    return nil
  }

  return child.p
}

func (pm *PointMap)Has(p *Point) bool {
  return pm.Find(p.x, p.y, p.z) != nil
}


//
//
// GRID
//
// a set of points

type Grid struct {
  points []*Point
  bounds *Bounds
  pmap *PointMap
}

func NewGrid() *Grid {
  return &Grid{[]*Point{}, nil, NewPointMap()}
}

func (g *Grid)AddPoint(p *Point) {
  if g.pmap.Find(p.x, p.y, p.z) == nil {
    g.points = append(g.points, p)
    if g.bounds == nil {
      g.bounds = &Bounds{p,p}
    } else {
      g.bounds.Expand(p)
    }

    g.pmap.AddPoint(p)
  }
}

func (g *Grid)Bounds() *Bounds {
  if len(g.points) == 0 {
    return nil
  }

  p := g.points[0]
  var minX, minY, minZ, maxX, maxY, maxZ int
  minX, maxY = p.x, p.x
  minY, maxY = p.y, p.y
  minZ, maxZ = p.z, p.z

  for i:=1; i < len(g.points); i++ {
    p = g.points[i]
    minX = util.Min(minX, p.x)
    maxX = util.Max(maxX, p.x)
    minY = util.Min(minY, p.y)
    maxY = util.Max(maxY, p.y)
    minZ = util.Min(minZ, p.z)
    maxZ = util.Max(maxZ, p.z)
  }

  return &Bounds{
    &Point{minX, minY, minZ},
    &Point{maxX, maxY, maxZ},
  }
}

func (g *Grid)WalkFaces(nodeFn func(p *Point)) {
  b := g.bounds
  var p *Point
  // front to back
  for y := b.minp.y; y <= b.maxp.y; y++ {
    for x := b.minp.x; x <= b.maxp.x; x++ {
      for z := b.minp.z; z <= b.maxp.z; z++ {
        p = g.pmap.Find(x,y,z)
        if p != nil {
          nodeFn(p)
        }
      }
    }
  }

  nodeFn(nil)

  // left to right
  for y := b.minp.y; y <= b.maxp.y; y++ {
    for z := b.minp.z; z <= b.maxp.z; z++ {
      for x := b.minp.x; x <= b.maxp.x; x++ {
        p = g.pmap.Find(x,y,z)
        if p != nil {
          nodeFn(p)
        }
      }
    }
  }

  nodeFn(nil)

  // bottom up
  for z := b.minp.z; z <= b.maxp.z; z++ {
    for x := b.minp.x; x <= b.maxp.x; x++ {
      for y := b.minp.y; y <= b.maxp.y; y++ {
        p = g.pmap.Find(x,y,z)
        if p != nil {
          nodeFn(p)
        }
      }
    }
  }

  nodeFn(nil)
}

func CheckVisit(newOpen []*Point, grid *Grid, next *Point, visited *PointMap, bounds *Bounds, faceCount *int) []*Point {
  if bounds.Contains(next) && !visited.Has(next) {
    if grid.pmap.Has(next) {
      *faceCount++
    } else {
      visited.AddPoint(next)
      return append(newOpen, next)
    }
  }

  return newOpen
}

func ShellSearch(open []*Point, grid, newGrid *Grid, visited *PointMap, bounds *Bounds, faceCount *int) {
  newOpen := []*Point{}
  var next *Point
  for _, p := range open {
    next = p.Neighbor(Left)
    newOpen = CheckVisit(newOpen, grid, next, visited, bounds, faceCount)
    next = p.Neighbor(Right)
    newOpen = CheckVisit(newOpen, grid, next, visited, bounds, faceCount)
    next = p.Neighbor(Up)
    newOpen = CheckVisit(newOpen, grid, next, visited, bounds, faceCount)
    next = p.Neighbor(Down)
    newOpen = CheckVisit(newOpen, grid, next, visited, bounds, faceCount)
    next = p.Neighbor(In)
    newOpen = CheckVisit(newOpen, grid, next, visited, bounds, faceCount)
    next = p.Neighbor(Out)
    newOpen = CheckVisit(newOpen, grid, next, visited, bounds, faceCount)
  }

  if len(newOpen) > 0 {
    ShellSearch(newOpen, grid, newGrid, visited, bounds, faceCount)
  }
}

func (g *Grid)ShellCount() int {
  newGrid := NewGrid()

  searchBounds := g.bounds.Grow(1,1,1)

  visited := NewPointMap()
  visited.AddPoint(searchBounds.minp)
  open := []*Point{searchBounds.minp}

  var count int

  ShellSearch(open, g, newGrid, visited, searchBounds, &count)
  return count
}

