package test

import (
  "bufio"
  "os"
  "fmt"
)

func WithScanner(filename string, fn func(*bufio.Scanner)) {
  f, err := os.Open(filename)
  if err != nil {
    panic(fmt.Sprintf("ERROR: %v:", err))
  }

  defer f.Close()

  scanner := bufio.NewScanner(f)

  fn(scanner)
}
