package stack

type Stack[T any] struct {
  s []T
}

func New[T any](item T) *Stack[T] {
  return &Stack[T]{[]T{item}}
}

func (stack *Stack[T]) Push(item T) {
  stack.s = append([]T{item}, stack.s...)
}

func (stack *Stack[T]) Pop() (item T) {
  if stack.IsEmpty() {
    return
  }

  item = stack.s[0]
  stack.s = stack.s[1:]

  return
}

func (stack *Stack[T]) IsEmpty() bool {
  return len(stack.s) == 0
}

func (stack *Stack[T]) Reverse() *Stack[T] {
  if stack.IsEmpty() {
    return stack
  }

  revStack := Stack[T]{[]T{}}
  for !stack.IsEmpty() {
    revStack.Push(stack.Pop())
  }

  return &revStack
}

func (stack *Stack[T]) Peek(def T) (item T) {
  if stack.IsEmpty() {
    return def
  }

  return stack.s[0]
}
