package day21

import (
  "fmt"
  "strings"
)

type Monkey struct {
  name, operation, left, right string
  resolved bool
  value int
}

type OpNode struct {
  operation string
  value int
  resolved bool
  left, right *OpNode
}

func ParseMonkey(monkey string) *Monkey {
  var value int
  var left, right, op string

  splits := strings.Split(monkey, ": ")
  name, valueStr := splits[0], splits[1]

  resolved := true
  _, err := fmt.Sscanf(valueStr, "%d", &value)

  if err != nil {
    resolved = false
    fmt.Sscanf(valueStr, "%s %s %s", &left, &op, &right)
  }

  return &Monkey{name, op, left, right, resolved, value}
}

func (m *Monkey)Print() {
  fmt.Printf("%s: ", m.name)
  if m.resolved {
    println(m.value)
  } else {
    fmt.Printf("%s %s %s\n", m.left, m.operation, m.right)
  }
}

func RunOp(op string, left, right int) int {
  switch op {
  case "+":
    return left + right
  case "-":
    return left - right
  case "/":
    return left / right
  case "*":
    return left * right
  }

  panic("unrecognised OP!")
}

func Resolve(name string, mmap map[string]*Monkey, verbose bool) int {
  m := mmap[name]

  if m.resolved {
    if verbose {
      fmt.Printf("%s yells '%d!'\n", name, m.value)
    }
    return m.value
  }

  if verbose {
    fmt.Printf("%s has to ask %s...\n", name, m.left)
  }
  left := Resolve(m.left, mmap, verbose)
  if verbose {
    fmt.Printf("... and %s has to ask %s\n", name, m.right)
  }
  right := Resolve(m.right, mmap, verbose)
  op := m.operation

  value := RunOp(op, left, right)
  if verbose {
    fmt.Printf("%s now knows her value and yells '%d!'\n", name, value)
  }

  m.value = value
  m.resolved = true

  return m.value
}

func ResolveOpTree(name string, mmap map[string]*Monkey, nmap map[string]*OpNode) *OpNode {
  m := mmap[name]

  if name == "humn" {
    return &OpNode{operation: "?"}
  }

  if m.resolved {
    return &OpNode{value: m.value, resolved: true}
  }

  op := m.operation
  if name == "root" {
    op = "="
  }

  left := nmap[m.left]
  if left == nil {
    left = ResolveOpTree(m.left, mmap, nmap)
    nmap[m.left] = left
  }

  right := nmap[m.right]
  if right == nil {
    right = ResolveOpTree(m.right, mmap, nmap)
    nmap[m.right] = right
  }

  if left.resolved && right.resolved && op != "=" {
    value := RunOp(op, left.value, right.value)
    m.value = value
    m.resolved = true
    return &OpNode{value: value, resolved: true}
  }

  return &OpNode{operation: op, left: left, right: right}
}

func (op *OpNode)Walk() {
  if op.resolved {
    print(op.value)
  } else if op.operation == "?" {
    print("something")
  } else {
    op.left.Walk()
    fmt.Printf(" %s ", op.operation)
    op.right.Walk()
  }
}

func (op *OpNode)Solve() int {
  if op.operation != "=" {
    panic("Cannot solve!")
  }

  if op.left.resolved {
    return op.right.doSolve(op.left.value)
  }

  return op.left.doSolve(op.right.value)
}

func (op *OpNode)doSolve(currentVal int) int {
  if op.operation == "?" {
    return currentVal
  }

  if op.left.resolved {
    val := opReverseLeft(op.operation, op.left.value, currentVal)
    return op.right.doSolve(val)
  }

  val := opReverseRight(op.operation, op.right.value, currentVal)
  return op.left.doSolve(val)
}

func opReverseLeft(op string, left, result int) int {
  switch op {
  case "+":
    return result - left
  case "-":
    return left - result
  case "/":
    return left / result
  case "*":
    return result / left
  }

  panic("unrecognised OP!")
}

func opReverseRight(op string, right, result int) int {
  switch op {
  case "+":
    return result - right
  case "-":
    return result + right
  case "/":
    return right * result
  case "*":
    return result / right
  }

  panic("unrecognised OP!")
}

