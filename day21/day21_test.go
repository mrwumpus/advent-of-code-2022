package day21

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

const exampleInput = `root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32`

func TestExample(t *testing.T) {
  mmap := map[string]*Monkey{}
  for _, monkey := range strings.Split(exampleInput, "\n") {
    m := ParseMonkey(monkey)
    mmap[m.name] = m
  }

  rootVal := Resolve("root", mmap, false)

  if rootVal != 152 {
    t.Fail()
  }

  mmap = map[string]*Monkey{}
  for _, monkey := range strings.Split(exampleInput, "\n") {
    m := ParseMonkey(monkey)
    mmap[m.name] = m
  }

  nmap := map[string]*OpNode{}

  tree := ResolveOpTree("root", mmap, nmap)
  //tree.Walk()
  result := tree.Solve()
  if result != 301 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    mmap := map[string]*Monkey{}
    for scanner.Scan() {
      txt := scanner.Text()
      m := ParseMonkey(txt)
      mmap[m.name] = m
    }

    rootVal := Resolve("root", mmap, false)

    expectedValue := 169525884255464
    if rootVal != expectedValue {
      t.Errorf("Expected ROOT to shout %d, but she shouted %d", expectedValue, rootVal)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    mmap := map[string]*Monkey{}
    for scanner.Scan() {
      txt := scanner.Text()
      m := ParseMonkey(txt)
      mmap[m.name] = m
    }

    nmap := map[string]*OpNode{}

    tree := ResolveOpTree("root", mmap, nmap)
    result := tree.Solve()

    expectedValue := 3247317268284
    if result != expectedValue {
      t.Errorf("I should have sent %d, but instead sent %d", expectedValue, result)
    }
  })
}
