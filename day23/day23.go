package day23

import (
  "aoc/util/graph"
)

type Elf struct {
  position *graph.Point
  considerations []graph.Direction
  currentConsideration int // index of selected direction
  willMove bool
}

type ElfWorld struct {
  elves []*Elf
}

func (ew *ElfWorld)PositionMap() map[string]bool {
  positions := map[string]bool{}
  for _, elf := range ew.elves {
    positions[elf.position.Print()] = true
  }
  return positions
}

func (w *ElfWorld)AddElf(x, y int) {
  considerations := []graph.Direction{
    graph.Up, graph.Down, graph.Left, graph.Right,
  }
  
  elf := Elf{graph.NewPoint(x,y), considerations, -1, false}

  w.elves = append(w.elves, &elf)
}

func (w *ElfWorld)ParseRow(row string, y int) {
  for x, ch := range row {
    if ch == '#' {
      w.AddElf(x, y)
    }
  }
}

func (w *ElfWorld)Print() {
  ps := make([]*graph.Point, len(w.elves))
  for i, e := range w.elves {
    ps[i] = e.position
  }

  graph.PrintPoints(ps, "#", []*graph.Point{}, "G")
}

func (e *Elf)considerDirection(posMap map[string]bool, dir graph.Direction) bool {
  neighbor1 := e.position.Neighbor(dir)
  var neighbor2, neighbor3 *graph.Point

  switch dir {
  case graph.Up, graph.Down:
    neighbor2 = neighbor1.Neighbor(graph.Right)
    neighbor3 = neighbor1.Neighbor(graph.Left)
  case graph.Right, graph.Left:
    neighbor2 = neighbor1.Neighbor(graph.Up)
    neighbor3 = neighbor1.Neighbor(graph.Down)
  }

  return !(posMap[neighbor1.Print()] || posMap[neighbor2.Print()] || posMap[neighbor3.Print()])
}

func (e *Elf)Consider(posMap map[string]bool) *graph.Point {
  considering := []int{}
  for i, dir := range e.considerations {
    if e.considerDirection(posMap, dir) {
      considering = append(considering, i)
    }
  }

  if len(considering) == 4 {
    e.currentConsideration = -1
    return nil
  }

  if len(considering) == 0 {
    e.currentConsideration = 0
  } else {
    e.currentConsideration = considering[0]
    e.willMove = true
  }
  return e.Consideration()
}

func (e *Elf)Consideration() *graph.Point {
  if e.currentConsideration < 0 || !e.willMove {
    return nil
  }

  return e.position.Neighbor(e.considerations[e.currentConsideration])
}

func (e *Elf)Reconsider() {
  /*
  if e.currentConsideration < 0 {
    return
  }
  */

  newList := []graph.Direction{}
  consideredDir := e.considerations[0]

  for _, consideration := range e.considerations {
    if consideration != consideredDir {
      newList = append(newList, consideration)
    }
  }

  newList = append(newList, consideredDir)
  e.considerations = newList
  e.currentConsideration = -1
  e.willMove = false
}

func (e *Elf)Move(moveSet map[string]int) (moved bool) {
  p := e.Consideration()
  if p != nil {
    if moveSet[p.Print()] == 1 {
      e.position = p
      moved = true
    }
  }
  e.Reconsider()
  return
}

func (world *ElfWorld)MoveElves() (somethingMoved bool) {
  moveSet := map[string]int{}
  posMap := world.PositionMap()
  for _, elf := range world.elves {
    p := elf.Consider(posMap)
    if p != nil {
      count := moveSet[p.Print()]
      moveSet[p.Print()] = count + 1
    }
  }

  for _, elf := range world.elves {
    elfMoved := elf.Move(moveSet)
    somethingMoved = somethingMoved || elfMoved
  }

  return
}

func (world *ElfWorld)Score() (score int) {
  ps := make([]*graph.Point, len(world.elves))
  for i, e := range world.elves {
    ps[i] = e.position
  }

  minp, maxp := graph.Bounds(ps)
  elfMap := world.PositionMap()
  for y := minp.Y(); y <= maxp.Y(); y++ {
    for x := minp.X(); x <= maxp.X(); x++ {
      p := graph.NewPoint(x,y)
      if !elfMap[p.Print()] {
        score++
      }
    }
  }

  return
}

func (w *ElfWorld)countMoves() (moves int) {
  somethingMoved := true
  for somethingMoved {
    somethingMoved = w.MoveElves()
    moves++
  }

  return
}
