package day23

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

const exampleMap = `....#..
..###.#
#...#.#
.#...##
#.###..
##.#.##
.#..#..`

func TestExample(t *testing.T) {
  world := ElfWorld{[]*Elf{}}

  for y, row := range strings.Split(exampleMap, "\n") {
    world.ParseRow(row, y)
  }

  for i := 0; i < 10; i++ {
    world.MoveElves()
  }

  score := world.Score()

  if score != 110 {
    t.Fail()
  }

  world = ElfWorld{[]*Elf{}}

  for y, row := range strings.Split(exampleMap, "\n") {
    world.ParseRow(row, y)
  }
  moves := world.countMoves()

  if moves != 20 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    world := ElfWorld{[]*Elf{}}
    var y int
    for scanner.Scan() {
      txt := scanner.Text()
      world.ParseRow(txt, y)
      y++
    }
    for i := 0; i < 10; i++ {
      world.MoveElves()
    }

    score := world.Score()

    expectedScore := 4336
    if score != expectedScore {
      t.Errorf("Expected to score %d, but scored %d", expectedScore, score)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    world := ElfWorld{[]*Elf{}}
    var y int
    for scanner.Scan() {
      txt := scanner.Text()
      world.ParseRow(txt, y)
      y++
    }

    score := world.countMoves()

    expectedScore := 1005
    if score != expectedScore {
      t.Errorf("Expected to score %d, but scored %d", expectedScore, score)
    }
  })
}
