package day15

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

var exampleInput = `Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3`

func readSensors(lines []string) []*Sensor {
  sensors := []*Sensor{}

  for _, line := range lines {
    sensor := ParseSensor(line)
    sensors = append(sensors, sensor)
  }

  return sensors
}

func TestExample(t *testing.T) {
  sensors := readSensors(strings.Split(exampleInput, "\n"))

  coveredOnRow := CountCoveredInRow(sensors, 10)

  if coveredOnRow != 26 {
    t.Errorf("Didn't count 26, counted %d", coveredOnRow)
  }

  possibleBeacons := UncoveredPointsWithinBounds(sensors, 0, 20)
  if len(possibleBeacons) != 1 {
    t.Errorf("Should have only found 1, but found %d", len(possibleBeacons))
  }

  if TuningFrequency(possibleBeacons[0]) != 56000011 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    lines := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        lines = append(lines, txt)
      }
    }

    sensors := readSensors(lines)

    coveredOnRow := CountCoveredInRow(sensors, 2000000)

    expectedCoverage := 5100463
    if coveredOnRow != expectedCoverage {
      t.Errorf("Didn't count %d, counted %d", expectedCoverage, coveredOnRow)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    lines := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        lines = append(lines, txt)
      }
    }

    sensors := readSensors(lines)

    possibleBeacons := UncoveredPointsWithinBounds(sensors, 0, 4000000)
    if len(possibleBeacons) != 1 {
      t.Errorf("Should have only found 1, but found %d", len(possibleBeacons))
    }

    expectedFreq := 11557863040754
    tuningFrequency := TuningFrequency(possibleBeacons[0])
    if tuningFrequency != expectedFreq {
      t.Errorf("Expected TuningFrequency of %d, but got %d", expectedFreq, tuningFrequency)
    }
  })
}

