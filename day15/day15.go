package day15

import (
  "fmt"
  "aoc/util/graph"
)

type Sensor struct {
  location, beacon *graph.Point
  d int
}

func ParseSensor(line string) *Sensor {
  var lx, ly, bx, by int

  fmt.Sscanf(line, "Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d", &lx, &ly, &bx, &by)
  lp := graph.NewPoint(lx, ly)
  bp := graph.NewPoint(bx, by)

  return &Sensor{lp, bp, lp.Distance(bp)}
}

func min(a int, b int) int {
  if a < b {
    return a
  }
  return b
}

func max(a int, b int) int {
  if a > b {
    return a
  }
  return b
}

func MinMaxX(sensors []*Sensor) (minx int, maxx int) {
  sensor := sensors[0]
  minx = sensor.location.X() - sensor.d
  maxx = sensor.location.X() + sensor.d

  for i := 1; i < len(sensors); i++ {
    sensor = sensors[i]
    minx = min(minx, sensor.location.X() - sensor.d)
    maxx = max(maxx, sensor.location.X() + sensor.d)
  }

  return
}

func PointIsCovered(sensors []*Sensor, x, y int) (retsensor *Sensor, hitBeacon bool) {
  p := graph.NewPoint(x, y)

  hitBeacon = false

  for _, sensor := range sensors {
    if !hitBeacon && sensor.beacon.Same(p) {
      hitBeacon = true
    }
    if retsensor == nil && p.WithinRange(sensor.location, sensor.d) {
      retsensor = sensor
    }
  }

  return
}

func CountCoveredInRow(sensors []*Sensor, row int) (count int) {
  minx, maxx := MinMaxX(sensors)

  for x := minx; x <= maxx; x++ {
    sensor, isBeacon := PointIsCovered(sensors, x, row)
    if sensor != nil && !isBeacon {
      count++
    }
  }

  return
}

func GetMaxRowPointOutOfRange(sensor *Sensor, row int) (x int) {
  offset := 0
  if row > sensor.location.Y() {
    offset = row - sensor.location.Y()
  } else {
    offset = sensor.location.Y() - row
  }

  return sensor.location.X() + sensor.d - offset + 1
}

func UncoveredPointsWithinBounds(sensors []*Sensor, minBound, maxBound int) []*graph.Point {
  points := []*graph.Point{}
  for y := minBound; y <= maxBound; y++ {
    x := minBound
    for x <= maxBound {
      sensor, _ := PointIsCovered(sensors, x, y)
      if sensor != nil {
        x = GetMaxRowPointOutOfRange(sensor, y)
      } else {
        points = append(points, graph.NewPoint(x,y))
        x++
      }
    }
  }

  return points
}

func TuningFrequency(p *graph.Point) int {
  return p.X() * 4000000 + p.Y()
}
