package day9

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

var exampleInput = []string{
  "R 4",
  "U 4",
  "L 3",
  "D 1",
  "R 4",
  "D 1",
  "L 5",
  "R 2",
}

var largerExample = []string{
  "R 5",
  "U 8",
  "L 8",
  "D 3",
  "R 17",
  "D 10",
  "L 25",
  "U 20",
}

func TestExample(t *testing.T) {
  tailPath := TrackTailPath(exampleInput)

  uniquePoints := CountUniquePoints(tailPath)

  if uniquePoints != 13 {
    t.Fail()
  }

  tailPath = TrackRopeTailPath(largerExample, 10)
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    inputs := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        inputs = append(inputs, txt)
      }
    }

    tailPath := TrackTailPath(inputs)
    uniquePoints := CountUniquePoints(tailPath)

    expectedPoints := 6357
    if uniquePoints != expectedPoints {
      t.Errorf("Expected %d, but got %d", expectedPoints, uniquePoints)
    }

  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    inputs := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        inputs = append(inputs, txt)
      }
    }

    tailPath := TrackRopeTailPath(inputs, 10)
    uniquePoints := CountUniquePoints(tailPath)

    expectedPoints := 2627
    if uniquePoints != expectedPoints {
      t.Errorf("Expected %d, but got %d", expectedPoints, uniquePoints)
    }

  })
}
