package day9

import (
  "fmt"
)

type Point struct {
  x, y int
}

type Direction int

const (
  Up = iota
  Down
  Left
  Right
)

func ParseDirection(dirStr string) Direction {
  switch dirStr {
  case "U":
    return Up
  case "D":
    return Down
  case "L":
    return Left
  default:
    return Right
  }
}

func ParseMovement(input string) (d Direction, steps int) {
  var dirStr string

  fmt.Sscanf(input, "%s %d", &dirStr, &steps)
  d = ParseDirection(dirStr)

  return
}

func (p *Point) Neighbor(dir Direction) *Point {
  switch dir {
  case Up:
    return &Point{p.x, p.y + 1}
  case Down:
    return &Point{p.x, p.y - 1}
  case Left:
    return &Point{p.x - 1, p.y}
  case Right:
    return &Point{p.x + 1, p.y}
  }

  return p
}

func (p *Point) Same(p2 *Point) bool {
  return p.x == p2.x && p.y == p2.y
}

func (p *Point) NextToOrSame(p2 *Point) bool {
  return p.x >= p2.x - 1 && p.x <= p2.x + 1 &&
         p.y >= p2.y - 1 && p.y <= p2.y + 1
}

func min(a int, b int) int {
  if a < b {
    return a
  }
  return b
}

func max(a int, b int) int {
  if a > b {
    return a
  }
  return b
}

func FindPointAt(rope *[]*Point, x int, y int) (int, *Point) {
  for i, p := range *rope {
    if p.x == x && p.y == y {
      return i, p
    }
  }
  return -1, nil
}

func PrintRope(rope *[]*Point) {

  p := (*rope)[0]

  minX, minY := min(0, p.x), min(0, p.y)
  maxX, maxY := max(0, p.x), max(0, p.y)

  for i := 1; i < len(*rope); i++ {
    p = (*rope)[i]
    minX = min(minX, p.x)
    maxX = max(maxX, p.x)
    minY = min(minY, p.y)
    maxY = max(maxY, p.y)
  }

  for y := maxY; y >= minY; y-- {
    for x := minX; x <= maxX; x++ {
      i, p := FindPointAt(rope, x, y)
      if p == nil {
        if x == 0 && y == 0 {
          print("s")
        } else {
          print(".")
        }
      } else if i == 0 {
        print("H")
      } else {
        print(i)
      }
    }
    println("")
  }
}

func StepRope (rope *[]*Point, dir Direction) *[]*Point {
  newRope := make([]*Point, len(*rope))

  prevHead := (*rope)[0]
  head := prevHead.Neighbor(dir)
  knot := prevHead

  newRope[0] = head

  for i := 1; i < len(*rope); i++ {
    knot = (*rope)[i]
    current := knot
    if ! current.NextToOrSame(head) {
      if head.x > current.x && head.y > current.y {
        current = current.Neighbor(Up).Neighbor(Right)
      } else if head.x > current.x && head.y < current.y {
        current = current.Neighbor(Down).Neighbor(Right)
      } else if head.x < current.x && head.y > current.y {
        current = current.Neighbor(Up).Neighbor(Left)
      } else if head.x < current.x && head.y < current.y {
        current = current.Neighbor(Down).Neighbor(Left)
      } else if head.x == current.x && head.y > current.y {
        current = current.Neighbor(Up)
      } else if head.x == current.x && head.y < current.y {
        current = current.Neighbor(Down)
      } else if head.x > current.x && head.y == current.y {
        current = current.Neighbor(Right)
      } else if head.x < current.x && head.y == current.y {
        current = current.Neighbor(Left)
      } else {
        current = prevHead
      }
    }

    prevHead = knot
    head = current
    newRope[i] = head
  }

  return &newRope
}

func MoveRopeCollectTail (rope *[]*Point, movement string) (*[]*Point, *[]*Point) {
  dir, steps := ParseMovement(movement)

  newRope := rope
  tailPoints := []*Point{}
  for i := 0; i < steps; i++ {
    newRope = StepRope(newRope, dir)
    tailPoints = append(tailPoints, last(newRope))
  }

  return newRope, &tailPoints
}

func (tail *Point) TailFollow(head *Point, movement string) (*Point, *[]*Point) {
  dir, steps := ParseMovement(movement)

  tailPath := []*Point{}

  curHead := head
  prevHead := curHead
  newTail := tail
  for i := 0; i < steps; i++ {
    curHead = curHead.Neighbor(dir)
    if ! newTail.NextToOrSame(curHead) {
      newTail = prevHead
    }

    prevHead = curHead

    tailPath = append(tailPath, newTail)
  }

  return curHead, &tailPath
}

func (p *Point) Str() string {
  return fmt.Sprintf("%d,%d", p.x, p.y)
}

func CountUniquePoints(pts *[]*Point) int {
  ptsMap := map[string]bool{}

  for _, p := range *pts {
    ptsMap[p.Str()] = true
  }

  return len(ptsMap)
}

func last(pts *[]*Point) *Point {
  return (*pts)[len(*pts) - 1]
}

func TrackTailPath(inputs []string) *[]*Point {
  head := &Point{0,0}
  tail := &Point{0,0}

  tailPath := []*Point{}
  var newPath *[]*Point

  for _, movement := range inputs {
    head, newPath = tail.TailFollow(head, movement)

    tailPath = append(tailPath, (*newPath)...)

    tail = last(&tailPath)
  }

  return &tailPath
}

func TrackRopeTailPath(inputs []string, knotCount int) *[]*Point {
  knots := make([]*Point, knotCount)
  for i := 0; i < knotCount; i++ {
    knots[i] = &Point{0,0}
  }

  tailPath := []*Point{}
  var newPath *[]*Point

  newKnots := &knots

  for _, movement := range inputs {
    newKnots, newPath = MoveRopeCollectTail(newKnots, movement)
    tailPath = append(tailPath, (*newPath)...)
    //PrintRope(newKnots)
    //println()
  }

  return &tailPath
}

func printPath(path *[]*Point) {
  for _, p := range *path {
    println(p.Str())
  }
}
