package day3

import (
  "testing"
  "bufio"
  //"strings"
  "aoc/util/test"
)

func TestExamples(t *testing.T) {
  examples := []struct {
    contents string
    commonItem rune
    priority int } {
      { "vJrwpWtwJgWrhcsFMMfFFhFp", 'p', 16},
      { "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", 'L', 38},
      { "PmmdzqPrVvPwwTWBwg", 'P', 42},
      { "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", 'v', 22},
      { "ttgJtRGJQctTZtZT", 't', 20},
      { "CrZsJsPPZsGzwwsLwLmpwMDw", 's', 19},
    }

    for _, example := range examples {
      commonItem := FindCommon(example.contents)

      if commonItem != example.commonItem {
        t.Errorf("Common item should be %q, but found %q", example.commonItem, commonItem)
      }

      priority := ItemPriority(commonItem)

      if priority != example.priority {
        t.Errorf("Item Priority should be %d, but found %d", example.priority, priority)
      }
    }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func (scanner *bufio.Scanner) {
    var scoreTotal int
    for scanner.Scan() {
      txt := scanner.Text()
      score := scoreCommonItemPriority(txt)
      scoreTotal += score
    }

    expected := 8109
    if scoreTotal != expected {
      t.Errorf("Total should be %d, but calculated %d", expected, scoreTotal)
    }
  })
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func (scanner *bufio.Scanner) {
    var scoreTotal int
    var first, second string

    for scanner.Scan() {
      txt := scanner.Text()
      if first != "" {
        if second != "" {
          badge := FindCommonBadge(first, second, txt)

          scoreTotal += ItemPriority(badge)
          first = ""
          second = ""
        } else {
          second = txt
        }
      } else {
        first = txt
      }
    }

    expected := 2738
    if scoreTotal != expected {
      t.Errorf("Total should be %d, but calculated %d", expected, scoreTotal)
    }
  })
}

func scoreCommonItemPriority(rucksack string) int {
  commonItem := FindCommon(rucksack)
  return ItemPriority(commonItem)
}
