package day3

const (
  NIL = '_'
)

func stringMap(val string) map[rune]bool {
  strMap := map[rune]bool{}
  for _, ch := range val {
    strMap[ch] = true
  }

  return strMap
}

func firstValue(stringMap map[rune]bool) rune {
  for key, value := range stringMap {
    if value {
      return key
    }
  }

  return NIL
}

func strIntersect(first string, second string) map[rune]bool {
  return intersect(stringMap(first), second)
}

func intersect(firstMap map[rune]bool, second string) map[rune]bool {
  intersection := map[rune]bool{}

  for _, ch := range second {
    if firstMap[ch] {
      intersection[ch] = true
    }
  }

  return intersection
}

func FindCommonBadge(groupOne string, groupTwo string, groupThree string) rune {
  intersection := intersect(strIntersect(groupOne, groupTwo), groupThree)

  return firstValue(intersection)
}

func FindCommon(rucksack string) rune {
  midpoint := len(rucksack) / 2
  compartmentOne := rucksack[:midpoint]
  compartmentTwo := rucksack[midpoint:]

  intersection := strIntersect(compartmentOne, compartmentTwo)
  return firstValue(intersection)
}

func ItemPriority(c rune) int {
  baseValue := int(c - 'A')
  if baseValue <= 25 {
    return baseValue + 27
  }

  return baseValue - 31
}
