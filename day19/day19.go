package day19

import (
  "fmt"
  "sort"
  "strings"
  "time"
  "aoc/util"
)

type Resource int

const (
  Ore Resource = iota
  Clay
  Obsidian
  Geode
)

func (r Resource)Name() string {
  switch r {
    case Ore: return "ORE"
    case Clay: return "CLAY"
    case Obsidian: return "OBSIDIAN"
    case Geode: return "GEODE"
  }

  return "UNKNOWN"
}

type Costs map[Resource]int

func (costs *Costs)Of(r Resource) int {
  return (*costs)[r]
}

func InitCosts(ore, clay, obsidian int) *Costs {
  costs := Costs{}
  costs[Ore] = ore
  costs[Clay] = clay
  costs[Obsidian] = obsidian

  return &costs
}

type State struct {
  resources, robots *map[Resource]int
  parent *State
  gen int
}

func (s *State)Print() {
  ForEachResource(func(r Resource) {
    fmt.Printf("%s: %d robots; %d resources", r.Name(), (*s.robots)[r], (*s.resources)[r])
  })
}

func InitialState() *State {
  robots := map[Resource]int{}
  robots[Ore] = 1
  return &State{&map[Resource]int{}, &robots, nil, 0}
}

type Factory struct {
  multiverse []*State
  costs map[Resource]*Costs
}

func StartFactory() *Factory {
  initialState := InitialState()
  return &Factory{[]*State{ initialState }, map[Resource]*Costs{}}
}

func (f *Factory)SetCosts(r Resource, ore, clay, obsidian int) {
  costs := InitCosts(ore, clay, obsidian)
  f.costs[r] = costs
}

func (f *Factory)BestResourceState(r Resource) (best *State) {
  var maxResources int
  for _, state := range f.multiverse {
    if (*state.resources)[r] > maxResources {
      best = state
      maxResources = (*state.resources)[r]
    }
  }

  return
}

func ForEachResource(resfn func(Resource)) {
  resfn(Geode)
  resfn(Obsidian)
  resfn(Clay)
  resfn(Ore)
}

func (f *Factory)StartBuild(s *State, r Resource) *State {
  costs := f.costs[r]

  oreCost, clayCost, obsidianCost := costs.Of(Ore), costs.Of(Clay), costs.Of(Obsidian)

  if oreCost <= (*s.resources)[Ore] &&
     clayCost <= (*s.resources)[Clay] &&
     obsidianCost <= (*s.resources)[Obsidian] {
    newResources := map[Resource]int{}
    newResources[Ore] = (*s.resources)[Ore] - oreCost
    newResources[Clay] = (*s.resources)[Clay] - clayCost
    newResources[Obsidian] = (*s.resources)[Obsidian] - obsidianCost
    newResources[Geode] = (*s.resources)[Geode]

    return &State{&newResources, s.robots, s, s.gen + 1}
  }

  return nil
}

func (s *State)NewGatherState() *State {
  newResources := map[Resource]int{}
  ForEachResource(func(r Resource) {
    newResources[r] = (*s.resources)[r]
  })

  for res, workers := range *s.robots {
    newResources[res] += workers
  }

  return &State{&newResources, s.robots, s, s.gen + 1}
}

func (s *State)GatherAndCompleteBuild(res Resource) {
  newRobots := map[Resource]int{}

  for res, workers := range *s.robots {
    (*s.resources)[res] += workers
  }

  ForEachResource(func(r Resource) {
    newRobots[r] = (*s.robots)[r]
  })

  newRobots[res]++

  s.robots = &newRobots
}

func (s *State)potential(r Resource, costs *map[Resource]*Costs, points int) int {
  var maxTurns int
  cost := (*costs)[r]

  for res, resCost := range *cost {
    current := (*s.resources)[res]
    workers := (*s.robots)[res]

    if workers == 0 {
      return 0
    }

    neededTurns := 0
    if current < resCost {
      neededTurns = (resCost - current) / workers
      if neededTurns * workers < resCost {
        neededTurns++
      }
    }

    maxTurns = util.Max(maxTurns, neededTurns)
  }

  if maxTurns == 0 {
    return -points/2
  }

  return points / maxTurns
}

func (s *State)PotentialScore(costs *map[Resource]*Costs) int {
  potentialScore := s.potential(Geode, costs, 12) + s.potential(Obsidian, costs, 5) + s.potential(Clay, costs, 2)
  //botScore := (*s.robots)[Ore] + ((*s.robots)[Clay] * 2) + ((*s.robots)[Obsidian] * 3) + ((*s.robots)[Geode] * 8)
  return potentialScore // + botScore
}

func (s *State)Score() int {
  botScore := (*s.robots)[Ore] + ((*s.robots)[Clay] * 2) + ((*s.robots)[Obsidian] * 5) + ((*s.robots)[Geode] * 11)

  resourceScore := (*s.resources)[Ore] + ((*s.resources)[Clay] * 2) + ((*s.resources)[Obsidian] * 3) + ((*s.resources)[Geode] * 100)

  return botScore * 10 + resourceScore
}

func (s *State)ScoreAt(gen int, costs *map[Resource]*Costs) int {
  cur := s
  if cur.gen < gen {
    panic("Nope.")
  }
  for cur.gen != gen {
    cur = cur.parent
  }

  return cur.PotentialScore(costs)
}

func (f *Factory)RunMinute() {
  newStates := []*State{}
  var newState *State
  for _, state := range f.multiverse {
    newState = state.NewGatherState()
    newStates = append(newStates, newState)

    ForEachResource(func(r Resource) {
      newState = f.StartBuild(state, r)
      if newState != nil {
        newStates = append(newStates, newState)
        newState.GatherAndCompleteBuild(r)
      }
    })
  }

  if len(newStates) > 3000 {
    sort.Slice(newStates, func(i,j int) bool {
      return newStates[i].Score() > newStates[j].Score()
    })

    newStates = newStates[:3000]
  }

  f.multiverse = newStates
}

func (f *Factory)RunState(curState *State, maxGen int, maxTime int64) {
  curTime := time.Now().Unix()
  if curState.gen == maxGen {
    //println("New state with score:", curState.Score())
    f.multiverse = append(f.multiverse, curState)
    if len(f.multiverse) > 50000 {
      //println("Trimming states...")
      sort.Slice(f.multiverse, func(i,j int) bool {
        return (*f.multiverse[i].resources)[Geode] > (*f.multiverse[j].resources)[Geode]
      })

      f.multiverse = f.multiverse[:100]
    }
    return
  }

  if curTime > maxTime {
    //println("Out of time...")
    return
  }

  newStates := []*State{}

  ForEachResource(func(r Resource) {
    newState := f.StartBuild(curState, r)
    if newState != nil {
      newState.GatherAndCompleteBuild(r)
      newStates = append(newStates, newState)
    }
  })
  newStates = append(newStates, curState.NewGatherState())

  sort.Slice(newStates, func(i,j int) bool {
    s1, s2 := newStates[i], newStates[j]
    return s1.PotentialScore(&f.costs) > s2.PotentialScore(&f.costs)
  })

  for _, newState := range newStates {
    f.RunState(newState, maxGen, maxTime)
  }
}

func ParseFactory(blueprint string) *Factory {
  robotStr := strings.Split(blueprint, ": ")[1]
  robots := strings.Split(robotStr, ".")

  f := StartFactory()

  var ore, clay, obsidian int

  fmt.Sscanf(robots[0], "Each ore robot costs %d ore", &ore)
  f.SetCosts(Ore, ore, clay, obsidian)

  fmt.Sscanf(robots[1], " Each clay robot costs %d ore", &ore)
  f.SetCosts(Clay, ore, clay, obsidian)

  fmt.Sscanf(robots[2], " Each obsidian robot costs %d ore and %d clay", &ore, &clay)
  f.SetCosts(Obsidian, ore, clay, obsidian)
  clay = 0

  fmt.Sscanf(robots[3], " Each geode robot costs %d ore and %d obsidian", &ore, &obsidian)
  f.SetCosts(Geode, ore, clay, obsidian)

  return f
}

func (f *Factory)RunFactory(minutes int) int {
  for i := 0; i < minutes; i++ {
    f.RunMinute()
  }

  state := f.BestResourceState(Geode)

  if state == nil {
    return 0
  }

  return (*state.resources)[Geode]
}

func (f *Factory)RunDepthFactory(minutes, maxSeconds int) int {
  maxTime := time.Now().Unix()
  maxTime += int64(maxSeconds)

  initial := f.multiverse[0]
  f.multiverse = []*State{}

  f.RunState(initial, minutes, maxTime)

  state := f.BestResourceState(Geode)

  if state == nil {
    return 0
  }

  return (*state.resources)[Geode]
}

func ScoreBlueprints(bps []string, rounds int) int {
  var total int

  for i, blueprint := range bps {
    f := ParseFactory(blueprint)
    geodeCount := f.RunFactory(rounds)

    total += geodeCount*(i+1)
  }

  return total
}

func ScoreDepthBlueprints(bps []string, rounds, time int) int {
  var total int

  for i, blueprint := range bps {
    f := ParseFactory(blueprint)
    geodeCount := f.RunDepthFactory(rounds, time)

    total += geodeCount*(i+1)
    println(i+1, "of", len(bps), "SCORE:", geodeCount)
  }

  return total
}
