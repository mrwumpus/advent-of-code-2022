package day19

import (
  "bufio"
  "strings"
  "testing"
  "aoc/util/test"
)

var blueprintOne = `
Blueprint 1:
 Each ore robot costs 4 ore.
 Each clay robot costs 2 ore.
 Each obsidian robot costs 3 ore and 14 clay.
 Each geode robot costs 2 ore and 7 obsidian.`

var blueprintTwo = `
Blueprint 2:
 Each ore robot costs 2 ore.
 Each clay robot costs 3 ore.
 Each obsidian robot costs 3 ore and 8 clay.
 Each geode robot costs 3 ore and 12 obsidian.`

func TestExample(t *testing.T) {
  bp1 := strings.ReplaceAll(blueprintOne, "\n", "")
  bp2 := strings.ReplaceAll(blueprintTwo, "\n", "")

  /*
  f := ParseFactory(bp1)
  geodeCount := f.RunDepthFactory(24, 20)

  expectedCount := 9
  if geodeCount != expectedCount {
    t.Errorf("Expect a max of %d geodes, but counted %d", expectedCount, geodeCount)
  }

  f = ParseFactory(bp2)
  geodeCount = f.RunDepthFactory(24, 20)

  expectedCount = 12
  if geodeCount != expectedCount {
    t.Errorf("Expect a max of %d geodes, but counted %d", expectedCount, geodeCount)
  }
  */

  total := ScoreDepthBlueprints([]string{bp1,bp2}, 24, 15)

  if total != 33 {
    t.Fail()
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    blueprints := []string{}
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        blueprints = append(blueprints, txt)
      }
    }

    totalTime := 9 * 59
    perTime := totalTime/len(blueprints)
    if perTime * len(blueprints) > 600 {
      println("Prepare to wait 10 min for search. Each BP time:", perTime)
    }

    total := ScoreDepthBlueprints(blueprints, 24, perTime)
    expectedTotal := 1291 // current minumum
    if total != expectedTotal {
      t.Errorf("Expect a score of %d, but got %d", expectedTotal, total)
    }

  })
}
