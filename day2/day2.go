package day2

const (
  ROCK = 0
  PAPER = 1
  SCISSORS = 2

  WIN = 6
  LOSE = 0
  DRAW = 3
)

func convertFromInput (input string) int {
  switch input {
  case "A", "X":
    return ROCK
  case "B", "Y":
    return PAPER
  case "C", "Z":
    return SCISSORS
  }
  return -1
}

func convertResultFromInput (input string) int {
  switch input {
  case "X":
    return LOSE
  case "Y":
    return DRAW
  case "Z":
    return WIN
  }
  return -1
}

func scoreMyRound (mine int, yours int) int {
  if mine == ROCK {
    switch yours {
    case ROCK:
      return DRAW
    case PAPER:
      return LOSE
    case SCISSORS:
      return WIN
    }
  } else if mine == PAPER {
    switch yours {
    case ROCK:
      return WIN
    case PAPER:
      return DRAW
    case SCISSORS:
      return LOSE
    }
  } else if mine == SCISSORS {
    switch yours {
    case ROCK:
      return LOSE
    case PAPER:
      return WIN
    case SCISSORS:
      return DRAW
    }
  }

  return -1
}

func determineMine (yours int, result int) int {
  switch result {
  case WIN:
    return (yours + 1) % 3
  case LOSE:
    return (yours + 2) % 3
  case DRAW:
    return yours
  }

  return -1
}

func ScorePart1Input (first string, second string) int {
    yours, mine := convertFromInput(first), convertFromInput(second)

    return mine + 1 + scoreMyRound(mine, yours)
}

func ScorePart2Inputs (yourInput string, resultInput string) int {
    yours, result := convertFromInput(yourInput), convertResultFromInput(resultInput)
    mine := determineMine(yours, result)

    return mine + 1 + result
}
