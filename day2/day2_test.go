package day2

import (
  "testing"
  "bufio"
  "strings"
  "aoc/util/test"
)

func TestPartOneExample(t *testing.T) {
  cases := []struct {
    first, second string
    expected int } {
      {"A", "Y", 8},
      {"B", "X", 1},
      {"C", "Z", 6},
    }
  for _, test := range cases {
    score := ScorePart1Input(test.first, test.second)
    if score != test.expected {
      t.Errorf("score should be %d, but got %d", test.expected, score)
    }
  }
}

func TestPartOneInput(t *testing.T) {
  test.WithScanner("input", func (scanner *bufio.Scanner) {
    var scoreTotal int

    for scanner.Scan() {
      txt := scanner.Text()
      inputs := strings.Split(txt, " ")
      score := ScorePart1Input(inputs[0], inputs[1])

      scoreTotal += score
    }

    if scoreTotal != 13005 {
      t.Errorf("Part1 total shold be 13005, but got %d", scoreTotal)
    }
  })
}

func TestPartTwoInput(t *testing.T) {
  test.WithScanner("input", func (scanner *bufio.Scanner) {
    var scoreTotal int

    for scanner.Scan() {
      txt := scanner.Text()
      inputs := strings.Split(txt, " ")
      score := ScorePart2Inputs(inputs[0], inputs[1])

      scoreTotal += score
    }

    if scoreTotal != 11373 {
      t.Errorf("Part2 total shold be 11373, but got %d", scoreTotal)
    }
  })
}
