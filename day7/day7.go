package day7

import (
  "fmt"
  "strings"
)

type FsNode struct {
  name string
  parent *FsNode
  contents *map[string]*FsNode
  size int
}

func NewFile(name string, parent *FsNode, size int) *FsNode {
  return &FsNode{ name, parent, nil, size }
}

func NewDir(name string, parent *FsNode) *FsNode {
  return &FsNode{ name, parent, &map[string]*FsNode{}, 0 }
}

func (node *FsNode) addFileSize(size int) {
  node.size += size
  if node.parent != nil {
    node.parent.addFileSize(size)
  }
}

func (node *FsNode) AddFile(name string, size int) *FsNode {
  if node.contents != nil {
    child := NewFile(name, node, size)
    (*node.contents)[name] = child
    node.addFileSize(size)
  }

  return node
}

func (node *FsNode) AddDir(name string) *FsNode {
  if node.contents != nil {
    child := NewDir(name, node)
    (*node.contents)[name] = child
  }

  return node
}

func (node *FsNode) Filter(path string, fn func(*FsNode) bool) (passingNodes map[string]*FsNode) {
  passingNodes = map[string]*FsNode{}

  if node.contents != nil {
    for name, child := range *node.contents {
      childPath := path + "/" + name
      if fn(child) {
        passingNodes[childPath] = child
      }

      for grandName, grandChild := range child.Filter(childPath, fn) {
        passingNodes[grandName] = grandChild
      }
    }
  }

  return
}

type Navigator struct {
  root, current *FsNode
}

func (nav *Navigator) cdRoot() {
  nav.current = nav.root
}

func (nav *Navigator) cd(name string) {
  if name == ".." {
    if nav.current != nil && nav.current.parent != nil {
      nav.current = nav.current.parent
    }
  } else {
    if nav.current != nil && nav.current.contents != nil {
      next, exists := (*nav.current.contents)[name]
      if exists {
        nav.current = next
      }
    }
  }
}

func ExecuteCommand(nav *Navigator, command string) {
  if strings.HasPrefix(command, "$") {
    if strings.HasPrefix(command, "$ cd") {
      var dirName string
      _, err := fmt.Sscanf(command, "$ cd %s", &dirName)
      if err != nil {
        panic(err)
      }
      if dirName == "/" {
        nav.cdRoot()
      } else {
        nav.cd(dirName)
      }
    }
  } else if strings.HasPrefix(command, "dir") {
    var name string
    _, err := fmt.Sscanf(command, "dir %s", &name)
    if err != nil {
      panic(err)
    }
    nav.current.AddDir(name)
  } else {
    var size int
    var name string
    _, err := fmt.Sscanf(command, "%d %s", &size, &name)
    if err != nil {
      panic(err)
    }
    nav.current.AddFile(name, size)
  }
}
