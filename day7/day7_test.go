package day7

import (
  "bufio"
  "testing"
  "aoc/util/test"
)

func TestExample(t *testing.T) {
  commands := []string {
    "$ cd /",
    "$ ls",
    "dir a",
    "14848514 b.txt",
    "8504156 c.dat",
    "dir d",
    "$ cd a",
    "$ ls",
    "dir e",
    "29116 f",
    "2557 g",
    "62596 h.lst",
    "$ cd e",
    "$ ls",
    "584 i",
    "$ cd ..",
    "$ cd ..",
    "$ cd d",
    "$ ls",
    "4060174 j",
    "8033020 d.log",
    "5626152 d.ext",
    "7214296 k",
  }

  root := NewDir("/", nil)
  nav := &Navigator{ root, nil }
  for _, cmd := range commands {
    ExecuteCommand(nav, cmd)
  }

  expectedRootSize := 48381165
  if root.size != expectedRootSize {
    t.Errorf("Total size should be %d, but got %d", expectedRootSize, root.size)
  }

  nodes := root.Filter("", func(n *FsNode) bool {
    return n.size <= 100000 && n.contents != nil
  })

  var total int
  for _, n := range nodes {
    total += n.size
  }

  expectedSum := 95437
  if expectedSum != total {
    t.Errorf("Sum for large dirs should be %d, but got %d", expectedSum, total)
  }
}

func TestPartOne(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    root := NewDir("/", nil)
    nav := &Navigator{ root, nil }
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        ExecuteCommand(nav, txt)
      }
    }

    nodes := root.Filter("", func(n *FsNode) bool {
      return n.size <= 100000 && n.contents != nil
    })

    var total int
    for _, n := range nodes {
      total += n.size
    }

    expectedSum := 1611443
    if expectedSum != total {
      t.Errorf("Sum for large dirs should be %d, but got %d", expectedSum, total)
    }
  })
}

func max(a int, b int) int {
  if a < b {
    return b
  }

  return a
}

func min(a int, b int) int {
  if a < b {
    return a
  }

  return b
}

func TestPartTwo(t *testing.T) {
  test.WithScanner("input", func(scanner *bufio.Scanner) {
    root := NewDir("/", nil)
    nav := &Navigator{ root, nil }
    for scanner.Scan() {
      txt := scanner.Text()
      if txt != "" {
        ExecuteCommand(nav, txt)
      }
    }

    diskSpace := 70000000
    neededSpace := 30000000
    totalNeededToBeFreed :=  max(0, neededSpace - (diskSpace - root.size))

    nodes := root.Filter("", func(n *FsNode) bool {
      return n.size >= totalNeededToBeFreed && n.contents != nil
    })

    minimum := -1
    for _, n := range nodes {
      if minimum == -1 {
        minimum = n.size
      } else {
        minimum = min(minimum, n.size)
      }
    }

    expectedMin := 2086088
    if expectedMin != minimum {
      t.Errorf("Minimum should be %d, but got %d", expectedMin, minimum)
    }
  })
}
